#include "tt_ilqg.h"

#ifdef _MEX
	#include "mex.h"
	#define printf  mexPrintf
#endif

#ifdef __APPLE__
#include <sys/time.h>
static double omp_get_wtime() {
	struct timeval now;
	gettimeofday(&now, 0);
	
	return (double)now.tv_sec + ((double)now.tv_usec)*1e-6;
}
#else
#include <omp.h>
#endif

// assumes T is a rollout (feasible)
mjoResult iLQG(void* cbData, trajectory* T, trajectory **candidates, mjOptOpt* options, ttNum* scratch, int scratchSize)
{
	// constants
	int	nu			= T->nu;	// control dimension
	int N			= T->N;	// trajectory length
	int nRollout	= MAX(1,MIN(MAX_THREADS, options->nRollout));				// number of line-search threads
	ttNum base		= tt_pow(options->minAlpha, 1.0 / MAX(1, nRollout - 1));	// linesearch reduction ratio

	// variables
	ttNum expected, improvement;
	double time;
	int i, j, winner, diverged, cb_error;

	/*---------------------------- derivatives -------------------*/
	time =	omp_get_wtime();
	cb_error = options->cb_derivatives(cbData, T);
	T->trace.timing[2] = 1000*(omp_get_wtime() - time);

	if ( cb_error )
		return T->trace.outcome = mjOPT_FAIL;

	/*-------------------------- back-pass -----------------------*/
	time =	omp_get_wtime();

	for( i=0; i<MAX(1,options->maxBpass); i++ )
	{
		diverged = back_pass(T, options->regType, scratch, scratchSize);
		if ( !diverged )
			break;
		else  // try to increase mu until no divergence
		{
			if( T->trace.mu >= options->muMax )
				break;
			muScale(&T->trace, *options, options->muFactor);
		}
	}

	T->trace.timing[3] += 1000*(omp_get_wtime() - time);

	if ( diverged )
	{
		tt_warning("negative definite Hessian: maximum mu reached");
		return T->trace.outcome = mjOPT_MUMAX;
	}

	// copy feedback set-points
	for( i=0; i<N; i++ )
		tt_copy(T->e[i].xk, T->e[i].x, T->nx);

	// quick return
	if ( !T->e[0].du) // a linear 'pontryagin' pass
		return T->trace.outcome = mjOPT_SUCCESS;

	/*-------------------------- parallel line-search ------------------------*/

	time =	omp_get_wtime();

	// prepare candidates
	for( j=0; j<nRollout; j++ )
	{
		copyTrajectory(candidates[j], T);

		// alpha = base^j = (1, base, base^2... minAlpha)
		candidates[j]->trace.alpha	= tt_pow(base, (ttNum)j);

		// clear modifiers
		candidates[j]->feedback		= 1;
		candidates[j]->time_shift	= 0;

		// u = alpha*du
		for( i=0; i<N; i++ )
			tt_scl(candidates[j]->e[i].u, T->e[i].du, candidates[j]->trace.alpha, nu);
	}

	// do linesearch
	winner = options->cb_rollout(cbData, T, candidates);

	/*---------------------------- accept/reject candidate ----------------------------------*/

	// check for improvement
	if( winner >= 0 )
		improvement =  T->trace.cost - candidates[winner]->trace.cost;
	else
		improvement = 0.0;

	if( improvement > 0 )
	{
		// success
		candidates[winner]->trace.outcome		= mjOPT_SUCCESS;
		candidates[winner]->trace.improvement	= improvement;

		// get reduction ratio
		expected = tt_max(0, -T->trace.alpha*( T->trace.dV[0] + T->trace.alpha*T->trace.dV[1] ) );
		candidates[winner]->trace.z = tt_min(tt_max(0,  T->trace.improvement / ( expected + ttMINVAL ) ), 2);

		// accept candidate
		copyTrajectory(T, candidates[winner]);
	}
	else
	{
		// failure
		T->trace.outcome		= mjOPT_NO_IMPROVEMENT;
		T->trace.improvement	= improvement;
		T->trace.alpha			= tt_sqrt(-1.0);
		T->trace.z				= tt_sqrt(-1.0);
	}

	T->trace.timing[4] += 1000*(omp_get_wtime() - time);

	/*---------------------------- modify mu ----------------------------------*/
	muControl(&T->trace, *options);

	return T->trace.outcome;
}

void prepareCandidates(trajectory* policy, trajectory **candidates, mjOptOpt* option, ttNum t0, const ttNum* x0)
{
	int i, j, k;
	int nx	= policy->nx;	// state dimension 
	int	nu	= policy->nu;	// control dimension
	int N	= policy->N;	// trajectory length

	// ============= copy initial state and time, erase controls
	for( i=0; i<option->nRollout; i++ )
	{
		tt_copy(candidates[i]->e[0].x, x0, nx);
		candidates[i]->e[0].t[0] = t0;
		for( j=0; j<N; j++ )
			tt_zero(candidates[i]->e[j].u, nu);
	}

	// ============= modify (feedback, time shift, noise)

	// first candidate: deterministic with feedback
	candidates[0]->feedback		= 1;
	candidates[0]->time_shift	= 0;

	// second candidate: deterministic with no feedback
	if ( option->nRollout > 1)
	{
		candidates[1]->feedback		= 0;
		candidates[1]->time_shift	= 0;
	}

	// (experimental) in the remaining rollouts: gaussian noise with partial feedback 
	// TODO: support callback to user-defined policy?
	for( i=2; i<option->nRollout; i++ )
	{
		candidates[i]->feedback		= .3; // parameter?
		candidates[i]->time_shift	= 0;
		for( j=0; j<N; j++ )
			for( k=0; k<nu; k++ )
				candidates[i]->e[j].u[k] = 0.1*policy->e[j].u[k]*tt_rand_gauss();  // parameter?
	}
}

// sanity check: the initial state and time are the same yet cost increased
void testCostIncrease(void* cbData, trajectory* policy, trajectory **candidates, mjOptOpt* option, int winner)
{
	// constants
	int nx	= policy->nx;	// state dimension 
	int	nu	= policy->nu;	// control dimension
	int	nc	= policy->nc;	// number of cost terms
	int N	= policy->N;	// trajectory length	
	// variables
	int aligned, i, j;
	// sanity check: the initial state and time are the same yet cost increased
	if ( option->verbosity == verboseWARNING || option->verbosity == verboseDEBUG )
	{
		// quick alignment check
		aligned = ( policy->e[0].x[0] == candidates[0]->e[0].x[0] ) &&
					( policy->e[0].t[0] == candidates[0]->e[0].t[0] );

		// full alignment check
		if ( aligned )
			for( i=0; (i<nx) && aligned; i++ )
				aligned  =  policy->e[0].x[i] == candidates[winner]->e[0].x[i];

		if ( aligned )
		{
			// throw warning
			if (policy->trace.cost + 1e-8 < candidates[winner]->trace.cost)
				tt_warning("initial state and time are the same yet cost increased!\n");

			// run debug sequence
			if ( option->verbosity == verboseDEBUG )
			{
				// report specific cost mismatch
				for( i=0; i<N; i++ )
					for( j=0; j<nc; j++ )
						if( policy->e[i].c[j] != candidates[winner]->e[i].c[j] )
							tt_warning("cost mismatch here!\n");

				// rollout again
				copyTrajectory(candidates[0], policy);
				for( j=0; j<N; j++ )
					tt_zero(candidates[0]->e[j].u, nu);
				candidates[0]->feedback = 0;
				winner = option->cb_rollout(cbData, policy, candidates);
			}
		}
	}
}

// assumes that candidates were prepared with x_0, t_0 (optionally: open-loop noise, rollout modifications)
mjoResult iLQGsolve(void* cbData, trajectory* T, trajectory **candidates, mjOptOpt* option, trajectory* T_temp)
{
	// constants
	int nx	= T->nx;					// state dimension 
	int	nu	= T->nu;					// control dimension
	int mn  = MAX(nx, nu);		
	int scratchSize = 11*mn + 7*mn*mn;	// scratch size for backpass

	// variables
	int i, winner;
	mjoResult result;
	double time;
	ttNum* scratch;

	// clear timers in T
	tt_zero(T->trace.timing, N_TRJ_TIMER);
	tt_zero(T->trace.timingCB[0], N_CB_TIMER*2);

	// copy trace
	for( i=0; i<option->nRollout; i++ )
		candidates[i]->trace = T->trace;

	// rollout the candidates
	time = omp_get_wtime();
	winner	= option->cb_rollout(cbData, T, candidates);
	T->trace.timing[1] += 1000*(omp_get_wtime() - time);

	// accept winner
	if( winner>=0 )
	{
		// sanity check
		testCostIncrease(cbData, T, candidates, option,  winner);

		// accept
		copyTrajectory(T, candidates[winner]);
		result = mjOPT_SUCCESS;
	}
	else
		result = mjOPT_DIVERGENCE;


	if ( result == mjOPT_SUCCESS )	
	{
		// save T in T_temp
		copyTrajectory(T_temp, T);

		// allocate scratch on heap. Yuval: bad idea?
		scratch = (ttNum*)tt_malloc(sizeof(ttNum)*scratchSize,8);

		for( i=0; i<option->maxIter; i++ )
		{
			result = MAX(result, iLQG(cbData, T, candidates, option, scratch, scratchSize));
			// accumulate T->improvement or keep just the last one?

			// check outcomes, print stuff, break if necessary
			// if ( result > mjOPT_SUCCESS )
			//		break;

			if( option->verbosity >= verboseITER )
				printf("  iteration: %3.0gms  | f: %-9.4g | df: %-9.3g  | log10mu: %4.1f | z: %5.2f | alpha: %5.2f | winner: %d\n",
					1000*(T->trace.timing[0]), T->trace.cost, T->trace.improvement,  tt_log(T->trace.mu)/tt_log(10.0), T->trace.z, T->trace.alpha, T->trace.winner);
		}
		// free scratch
		tt_free(scratch);
		// use T->winner to encode the rollout winner (rather than linesearch winner)
		T->trace.winner = winner;
	}
	
	// get total solve time
	T->trace.timing[0] = 1000*(omp_get_wtime() - time);

	// revert to previous policy
	if ( result < mjOPT_SUCCESS )
	{
		printf("time = %-6.2f no improvement, keeping previous policy.\n", T->e[0].t[0]);
		T_temp->trace = T->trace;
		copyTrajectory(T, T_temp);
	}

	// print stuff
	if( option->verbosity > verboseSILENT )
		switch ( result )
		{
		case mjOPT_DIVERGENCE:
			printf("time = %-6.2f    All rollouts diverged.\n", T->e[0].t[0]);
			break;
		case mjOPT_MUMAX:
			printf("time = %-6.2f    Backpass failed with mu = %g.\n", T->e[0].t[0], T->trace.mu);
			break;
		case mjOPT_FAIL:
			printf("Callback failure.\n");
			break;
		default:
			if( option->verbosity >= verboseFINAL )
				printf("time: %4.0fms | f: %-9.4g | df: %-9.3g  | log10mu: %5.1f | z: %5.2f | alpha: %5.2f | winner: %d\n",
					T->trace.timing[0], T->trace.cost, T->trace.improvement,  tt_log(T->trace.mu)/tt_log(10.0), T->trace.z, T->trace.alpha, T->trace.winner);

			if( option->verbosity == verboseDEBUG )
				for( i=0; i<T->N; i++ )
					if ( tt_isBad(T->e[0].u, T->nu) )
						printf("time = %-6.2f    Nans in controls at timestep %d.\n", T->e[0].t[0], i);
		}

	return result;
}



/*--------- back_pass: iterate Ricatti equation, compute a new policy ----------
 inputs:
	T		 	- trajectory
	scratch		- scratch space
	szScratch	- size of scratch space  >=   11*max(m,n) + 7*max(m,n)^2
----------------------------------- back_pass --------------------------------*/
int back_pass(trajectory *T, int regType, ttNum* scratch, int szScratch)
{
	int	szBackPass, szBoxQP, szRiccati;
	int i, k, diverge;
	ttNum *lower_u, *upper_u, gradnorm=0, unorm=0;
	ttNum *Qx, *Qu, *Qxx, *Qxu, *Quu;
	trajElement e, e_next;
	int N	= T->N,		// number of timesteps
		n	= T->ndx,	// state derivative dimension
		m	= T->nu;	// control dimension

	/*------------------------------ initialize ------------------------------*/

	// check scratch size
	szBackPass	= 2*m;								// lower_u, upper_u
	szBoxQP		= m*m + 7*m;						// boxQP
	szRiccati   = n*m + m*m + 3*MAX(m,n)*MAX(m,n);  // Qxu_reg, Quu_reg, tmp, tmp2, tmp3
	if ( !(T->flg & trHAMILTONIAN) )
		szRiccati	+= n + m + n*n + n*m + m*m;		// Qx, Qu, Qxx, Qxu, Quu
	if( szScratch < szBackPass + szBoxQP + szRiccati)
		tt_error("back_pass - insufficient scratch size");

	// allocate control limits
	lower_u  = T->lower[0]<T->upper[0] ? scratch : 0; 	scratch += m;
	upper_u  = T->lower[0]<T->upper[0] ? scratch : 0; 	scratch += m;


	if ( !(T->flg & trHAMILTONIAN) ){	// allocate locally for Qzz
		Qx      = scratch;	scratch+= n;
		Qu		= scratch;	scratch+= m;
		Qxx     = scratch;	scratch+= n*n;
		Qxu     = scratch;	scratch+= n*m;
		Quu     = scratch;	scratch+= m*m;
	}

	// set final cost-to-go = final cost
	tt_copy(T->e[N-1].Vx,   T->e[N-1].cx,   n);
	tt_copy(T->e[N-1].Vxx,  T->e[N-1].cxx, n*n);

	// initialize Armijo terms
	T->trace.dV[0] = T->trace.dV[1] = 0;

	/*------------------------------- main loop ------------------------------*/
	for (k=N-2; k>-1; k--)
	{
		e		= T->e[k];
		e_next	= T->e[k+1];

		// set limits for control modification
		if ( lower_u )
			tt_sub(lower_u, T->lower, e.u, m);
		if ( upper_u )
			tt_sub(upper_u, T->upper, e.u, m);

		if ( T->flg & trHAMILTONIAN ){	// allocate Qzz in T
			Qx		= e.Qx;
			Qu		= e.Qu;
			Qxx		= e.Qxx;
			Qxu		= e.Qxu;
			Quu		= e.Quu;
		}

		// Riccati step
		diverge = riccati(  n, m, T->trace.mu, regType,
							e_next.Vx, e_next.Vxx,
							e.yx, e.yu,	e.cx, e.cu, e.cxx, e.cxu, e.cuu,
							lower_u, upper_u, e_next.du,
							e.Vx, e.Vxx, e.du, e.K,
							T->trace.dV,
							Qx, Qu, Qxx, Qxu, Quu,
							scratch );

		if ( diverge )
			break; // step failed
	}


	if ( !diverge )
		if ( T->e[0].du )
		{
			for (i=0; i<N-1; i++)
			{
				gradnorm	+= tt_dot(T->e[i].du, T->e[i].du, m);
				unorm		+= tt_dot(T->e[i].u, T->e[i].u, m);
			} 
			T->trace.gradnorm	= tt_sqrt(gradnorm) / (1 + tt_sqrt(unorm)); // relative du norm
		}
		else
		{
			for (i=0; i<N-1; i++)
				gradnorm	+= tt_dot(T->e[i].Qu, T->e[i].Qu, m);
			T->trace.gradnorm	= tt_sqrt(gradnorm); // gradient norm
		}

	// returns the failed timestep or 0 on success
	return( k+1 );
}



/*------------------------ riccati: a single LQR step --------------------------
	computes a single step of the Riccati LQR formula, with regularization.
	assumes preallocated memory, see below.

SIZES
	n				- dimension of the (tangent space of the) state x
	m				- dimension of the (tangent space of the) state u

OPTIONS
	mu 				- regularisation parameter
	regType			- regularisation type  1: control  2: state

INPUTS
	value at the next timestep
		Wx			- n
		Wxx			- n*n
	first derivatives, states and controls
		yx			- n
		yu			- m
		cx			- n
		cu			- m
	second derivatives, controls
		cxx			- n*n
		cxu			- n*m
		cuu			- m*m
		cuu			- m*m
	box limits on the controls
		lower		- m
		upper		- m
	optional warm-start for the QP solver
		next_du		- m

OUTPUTS
	value function
		Vx			- n
		Vxx			- n*n
	proposed control modification
		du			- m
	control feedback matrix
		K 			- m*n
	Armijo coefficients
		dV			- 2
	Q function
		Qx			- n
		Qu			- m
		Qxx			- n*n
		Qxu			- n*m
		Quu			- m*m

SCRATCH SPACE
	scratch			- must be >= sizeof(ttNum) * ( 7*m + 2*m*m+ n*m + 3*MAX(m,n)*MAX(m,n) )

----------------------------------- riccati ----------------------------------*/
int riccati(int n, int m, ttNum mu, int regType,
			const ttNum *Wx, const ttNum *Wxx,
			const ttNum *yx, const ttNum *yu,
			const ttNum *cx, const ttNum *cu,
			const ttNum *cxx, const ttNum *cxu, const ttNum *cuu,
			const ttNum* lower, const ttNum* upper, const ttNum *next_du,
			ttNum *Vx, ttNum *Vxx, ttNum *du, ttNum *K,
			ttNum *dV,
			ttNum *Qx, ttNum *Qu, ttNum *Qxx, ttNum *Qxu, ttNum *Quu,
			ttNum *scratch)
{
	int i, j, szBoxQP, mmn = MAX(m,n);
	int result, mFree;
	ttNum *QPscratch, *Quu_reg, *Qxu_reg, *tmp, *tmp2, *tmp3;
	ttNum *R, *index;

	// allocate workspace variables
	szBoxQP		= m*m + 7*m;
	QPscratch	= scratch;	scratch+= szBoxQP;
	Qxu_reg		= scratch;	scratch+= n*m;
	Quu_reg		= scratch;	scratch+= m*m;
	tmp			= scratch;	scratch+= mmn*mmn;
	tmp2		= scratch;	scratch+= mmn*mmn;
	tmp3		= scratch;	scratch+= mmn*mmn;

	//---------------------- compute Qu,Qxu,Quu,Qx,Qxx -------------------------

	//    tmp  = yx'*Wxx
	tt_mulMatTMat(tmp, yx, Wxx, n, n, n);

	//#pragma omp parallel sections num_threads(2)
	{
	   //#pragma omp section
	   {
			//    Qx = cx + yx'*Wx
			tt_mulMatTVec(Qx, yx, Wx, n, n);
			tt_addTo(Qx, cx, n);

			//    Qxx = cxx + tmp*yx
			tt_mulMatMat(Qxx, tmp, yx, n, n, n);
			tt_addTo(Qxx, cxx, n*n);
	   }
	   //#pragma omp section
	   {
			//    Qu  = cu + yu'*Wx;
			tt_mulMatTVec(Qu, yu, Wx, n, m);
			tt_addTo(Qu, cu, m);

			//    Qxu = cxu  + tmp*yu;
			tt_mulMatMat(Qxu, tmp, yu, n, n, m);
			tt_addTo(Qxu,cxu,n*m);

			//    Quu = cuu + yu'*Wxx*yu;
			tt_mulMatTMat(tmp2, yu, Wxx, n, m, n);
			tt_mulMatMat(Quu, tmp2, yu, m, n, m);
			tt_addTo(Quu, cuu, m*m);

			//----------------------------- regularize ---------------------------------
			tt_copy(Qxu_reg, Qxu, n*m);
			tt_copy(Quu_reg, Quu, m*m);
			if ( mu ){
				if (regType == 1){
					tt_mulMatTMat(tmp, yx, yu, n, n, m);
					tt_addToScl(Qxu_reg, tmp, mu, m*n);	 // Qxu_reg = Qxu + mu*yx'*yu
					tt_mulMatTMat(tmp, yu, yu, n, m, m);
					tt_addToScl(Quu_reg, tmp, mu, m*m);	 // Quu_reg = Quu + mu*yu'*yu
				}else
					for( i=0; i<m; i++ )
						Quu_reg[i*m+i] += mu;			 // Quu_reg = Quu + mu*eye(m)
			}

			//--------------------------- find control law -----------------------------
			// solve boxQP (and factorize)
			mFree	= m;

			if ( du )
				result	= boxQP(&mFree, du, &R, &index, // outputs
								QPscratch,	szBoxQP,	// scratch space
								Quu_reg,	Qu,			// quadratic program
								lower, upper,			// bounds
								next_du,	0);			// options
			else
				result = 5;
	   }
	}

	// check for misconvergnce
	if (result < 3)
		return( 1 );

	// feedback gains K
	tt_zero(K, n*m);
	if ( du )
	{
		if (mFree == m)				// no dimensions are clamped
		{
			tt_cholBacksub(tmp, R, Qxu_reg, m, n, 0);
			tt_sub(tmp2,tmp,K,m*n);
			tt_transpose(K, tmp, n, m);
			tt_scl(K, K, -1, m*n);
		}
		else						// some dimensions are clamped
			if (mFree > 0)			// not all dimensions are clamped
			{
				// tmp = compress_free(Qxu)
				for ( i=0; i < mFree; i++ )
					for ( j=0; j<n; j++)
						tmp[mFree*j + i] = Qxu_reg[m*j + (int)index[i] ];

				// tmp2 = H_free\Qxu_free
				tt_cholBacksub(tmp2, R, tmp, mFree, n, 0);

				// K = expand_free(-tmp2)'
				for ( i=0; i < mFree; i++ )
					for ( j=0; j<n; j++)
						K[j + n*(int)index[i]] = -tmp2[j*mFree + i];
			}
	}

	//-------------------------- update cost-to-go -----------------------------

	// copy uncontrolled derivatives
	tt_copy(Vx,  Qx,  n);
	tt_copy(Vxx, Qxx, n*n);

	// add controlled derivatives
	if ( du )
	{
		// dV = dV + [du'*Qu  .5*du'*Quu*du]
		dV[0] +=  tt_dot(du, Qu, m);
		tt_mulMatVec(tmp, Quu, du, m, m);
		dV[1] +=  0.5 * tt_dot(du, tmp, m);

		// Vx += K'*(Quu*du + Qu) + Qxu'*du
		tt_add(tmp2, tmp, Qu, m); //reuse tmp=Quu*du
		tt_mulMatTVec(tmp, K, tmp2, m, n);
		tt_addTo(Vx, tmp, n);
		tt_mulMatVec(tmp, Qxu, du, n, m);
		tt_addTo(Vx, tmp, n);

		//#pragma omp parallel sections num_threads(2)
		{
		   //#pragma omp section
		   {	// tmp3 = K'*Quu*K
				tt_mulMatMat(Qxu_reg, Quu, K, m, m, n); //use Qxu_reg as tmp
				tt_mulMatTMat(tmp3, K, Qxu_reg, m, n, n);
		   }
		   //#pragma omp section
		   {	// tmp = Qxu*K + K'*Qxu'
				tt_sqrMatMat(tmp, Qxu, K, n, m, tmp2, mmn*mmn);
		   }
		}


		// Vxx += K'*Quu*K + Qxu*K + K'*Qxu'
		tt_addTo(Vxx, tmp3, n*n);
		tt_addTo(Vxx, tmp, n*n);

		#ifdef ttVERBOSE
		if( tt_isBad(Vxx, n*n) )	tt_warning("(riccati) vxx is bad\n");
		#endif
	}

	tt_symmetrize(Vxx, n);

	return(0);
}

// dt_start for n0 timestep
// then linearly interpolate to dt for nslope timesteps
// then use dt
void set_dt(trajectory *T, const mjOptOpt optopt, ttNum dt)
{
	ttNum dt0 = optopt.dt_start;
	int		n0 =optopt.n_dt_start, m;
	int		nslope =optopt.n_dt_interp;
	int		N = T->N;
	int i;
	if (dt0 && n0)
	{
		m = MIN(n0,T->N);
		if (nslope)
		{
			for( i=0;i<m;i++ )
				T->e[i].dt[0] = dt0;

			for( i=m;i<MIN(m+nslope,T->N);i++ )
				T->e[i].dt[0] = dt0 + (dt-dt0)*(i-m+1)/(nslope+1);

			for( i=m+nslope;i<T->N;i++ )
				T->e[i].dt[0] = dt;
		}
		else
		{
			for( i=0;i<m;i++ )
				T->e[i].dt[0]=dt0;
			for( i=m;i<T->N;i++ )
				T->e[i].dt[0]=dt;
		}
	}
	else
		for( i=0;i<N;i++ )
			T->e[i].dt[0] = dt;

	// set time
	for( i=1;i<N;i++ )
		T->e[i].t[0] = T->e[i-1].t[0] + T->e[i-1].dt[0];
}


void muScale(mjoTrace *t, const mjOptOpt o, ttNum factor)
{
	// scale dmu
	if (factor > 1)
		t->dmu	= tt_max(t->dmu*factor, factor);
	else
		t->dmu	= tt_min(t->dmu*factor, factor);
	// scale mu
	t->mu	= tt_min(tt_max(t->mu*t->dmu  , o.muMin), o.muMax);
}

void muControl(mjoTrace *t, const mjOptOpt o)
{
	// divergence or no improvement: increase dmu by muFactor^2
	if ( tt_isBad1(t->z) || tt_isBad1(t->alpha))
		muScale(t, o, o.muFactor*o.muFactor);

	// sufficient improvement: decrease dmu by muFactor
	else if ( t->z > o.zGood || t->alpha > o.alphaGood )
		muScale(t, o, 1/o.muFactor);

	// insufficient improvement: increase dmu by muFactor
	else if ( t->z < o.zBad || t->alpha < o.alphaBad )
		muScale(t, o, o.muFactor);

}

