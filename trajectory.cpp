#include "tt_trajectory.h"
#include "tt_util.h"
#include "string.h"
#include <math.h>
#include <stdio.h>

void trjSizes(int sz[N_TRJ][2], int nx, int nu, int nf, int nr, int nc, int ndx, int nuser)
{
	// size of sub-elements
	sz[0][0]	= 1;				sz[0][1]	= 1;			// t
	sz[1][0]	= 1;				sz[1][1]	= 1;			// dt
	sz[2][0]	= nx;				sz[2][1]	= 1;			// x
	sz[3][0]	= nu;				sz[3][1]	= 1;			// u
	sz[4][0]	= nuser;			sz[4][1]	= 1;			// user
	sz[5][0]	= nr;				sz[5][1]	= 1;			// r
	sz[6][0]	= nc;				sz[6][1]	= 1;			// c
	sz[7][0]	= nf;				sz[7][1]	= 1;			// f
	sz[8][0]	= ndx;				sz[8][1]	= ndx;			// yx
	sz[9][0]	= ndx;				sz[9][1]	= nu;			// yu
	sz[10][0]	= nr;				sz[10][1]	= ndx+nu;		// rz
	sz[11][0]	= ndx;				sz[11][1]	= 1;			// cx
	sz[12][0]	= nu;				sz[12][1]	= 1;			// cu
	sz[13][0]	= ndx;				sz[13][1]	= ndx;			// cxx
	sz[14][0]	= ndx;				sz[14][1]	= nu;			// cxu
	sz[15][0]	= nu;				sz[15][1]	= nu;			// cuu
	sz[16][0]	= nu;				sz[16][1]	= ndx;			// K
	sz[17][0]	= nx;				sz[17][1]	= 1;			// xk
	sz[18][0]	= nu;				sz[18][1]	= 1;			// du
	sz[19][0]	= ndx;				sz[19][1]	= 1;			// Vx
	sz[20][0]	= ndx;				sz[20][1]	= ndx;			// Vxx
	sz[21][0]	= ndx;				sz[21][1]	= 1;			// Qx
	sz[22][0]	= nu;				sz[22][1]	= 1;			// Qu
	sz[23][0]	= ndx;				sz[23][1]	= ndx;			// Qxx
	sz[24][0]	= ndx;				sz[24][1]	= nu;			// Qxu
	sz[25][0]	= nu;				sz[25][1]	= nu;			// Quu
};

const char *trjElement_names[N_TRJ] = {"t","dt","x","u","user","r","c","f",
								"yx","yu","rz","cx","cu","cxx","cxu","cuu",
								"K","xk","du","Vx","Vxx","Qx","Qu","Qxx","Qxu","Quu"};

// allocate and initialize trajectory structure
trajectory* newTrajectory(int nx, int nu, int nf, int nr, int nc, int ndx, int nuser, int N, int flg)
{
	int i, sz[N_TRJ][2], size;
	ttNum NaN = tt_sqrt(-1.0);

	// allocate trajectory
	trajectory *T;

	// check length
	if( !N )
		return NULL;

	// allocate trajectory
	T	= (trajectory*)tt_malloc(sizeof(trajectory),8);

	trjSizes(sz, nx, nu, nf, nr, nc, ndx, nuser);

	// save allocation flags
	T->flg	= flg;

	// initialize optimization trace
	T->trace.dV[0]		= NaN;
	T->trace.dV[1]		= NaN;
	T->trace.mu			= 1;
	T->trace.dmu		= 1;
	T->trace.alpha		= NaN;
	T->trace.z			= NaN;
	T->trace.cost		= NaN;
	T->trace.improvement= NaN;
	T->trace.winner		= -1;
	T->trace.policy_lag = 0;
	tt_fill(T->trace.timing, NaN, N_TRJ_TIMER);
	tt_fill(T->trace.timingCB[0], NaN, N_CB_TIMER*2);

	// rollout modifiers
	T->feedback			= 1;
	T->time_shift		= 0;

	// set global dimensions
	T->nx		= nx;
	T->nu		= nu;
	T->nf		= nf;
	T->nc		= nc;
	T->nr		= nr;
	T->ndx		= ndx;
	T->nuser	= nuser;
	T->N		= N;

	// allocate struct-array of N trjs
	T->e	= (trajElement*)tt_malloc(sizeof(trajElement)*N,8);
	memset(T->e, 0, sizeof(trajElement)*N);

	// compute size of a single trajectory element
	size	= 0;
	for (i=0; i<N_TRJ; i++)
		if ( flg & (1<<i) )
			size += sz[i][0]*sz[i][1] ;

	if( !size )
		tt_error("cannot allocate empty trajectory");

	// total size of N elements PLUS lower and upper
	T->szBuffer		= size*N + 2*nu;

	// allocate buffer
	T->buffer = (ttNum*)tt_malloc(T->szBuffer * sizeof(ttNum),8);
	if( !T->buffer )
		tt_error("could not allocate buffer");

	// initialize with NaNs
	for (i=0; i<T->szBuffer; i++)
		T->buffer[i]=0; //NaN;

	// set pointers
	setPtrTrajectory(T);

	// make sure time is increasing
	if (flg & trTIME)
		for (i=0; i<N; i++)
			T->e[i].t[0] = (ttNum)i;

	return T;
}

// point the pointers in T->e to T->buffer
void setPtrTrajectory(trajectory* T)
{
	int i, j;
	int sz[N_TRJ][2], s;

	// cursor pointer
	ttNum *p = T->buffer;

	trjSizes(sz, T->nx, T->nu, T->nf, T->nr, T->nc, T->ndx, T->nuser);

	// set pointers to control limits
	T->lower	= p;	p+= T->nu;
	T->upper	= p;	p+= T->nu;

	// set the pointers of the trajElement's
	for(i=0; i<T->N; i++)
		for(j=0; j<N_TRJ; j++)
		{
			if ( T->flg & (1<<j) )
			{
				s	= sz[j][0]*sz[j][1];		// is the case s == 0 a problem ?
				*(&(T->e[i].t) + j ) 	= p;	// treat structure like array
				p 						+= s;	// increment pointer
			}
		}

	// check size
	if ( (int)(p - T->buffer) != T->szBuffer )
		tt_error("trajectory buffer size mismatch");
}

// copy data between two trajectories
void copyTrajectory(trajectory* Tout, const trajectory* Tin)
{
	int i, j, Nmin;
	int nx		= Tin->nx,		// state
		nu		= Tin->nu,		// control
		nf		= Tin->nf,		// features
		nc		= Tin->nc,		// number of cost terms
		nr		= Tin->nr,		// residual
		ndx		= Tin->ndx,		// state derivative
		nuser	= Tin->nuser;	// user
	ttNum *p_in, *p_out;
	int sz[N_TRJ][2], size;

	if ( !Tout )
		return;

	if ( nx != Tout->nx || nu != Tout->nu || nf != Tout->nf || nc != Tout->nc || nr != Tout->nr || ndx != Tout->ndx || nuser != Tout->nuser )
		tt_error("can only copy trajectories of identical basic sizes");

	trjSizes(sz, nx, nu, nf, nr, nc, ndx, nuser);

	// copy constants
	Tout->trace			= Tin->trace;
	Tout->feedback		= Tin->feedback;
	Tout->time_shift	= Tin->time_shift;

	// copy limits
	tt_copy(Tout->lower, Tin->lower, nu);
	tt_copy(Tout->upper, Tin->upper, nu);

	// copy data
	Nmin = MIN(Tin->N, Tout->N);
	for( i=0; i<Nmin; i++ )
		for(j=0; j<N_TRJ; j++)
		{
			p_in	= *( &(Tin->e[i].t)  + j );	// treat structure like array
			p_out	= *( &(Tout->e[i].t) + j );	// treat structure like array
			size	= sz[j][0]*sz[j][1];
			if ( p_in &&  p_out)
			{
				if ( p_out+size > Tout->buffer+Tout->szBuffer)
					tt_error("copy exceeds buffer allocation");
				tt_copy(p_out,	p_in, size);
			}
		}
}

trajectory* cloneTrajectory(const trajectory* Tin, int flg)
{
	trajectory* Tout;
	if ( !Tin )
		return NULL;
	Tout = newTrajectory(Tin->nx, Tin->nu, Tin->nf, Tin->nr, Tin->nc, Tin->ndx, Tin->nuser, Tin->N, flg);
	copyTrajectory(Tout, Tin);
	return Tout;
}

// interpolate trajectory at time t
void interpTrajectory(trajElement* e, const trajectory* T, ttNum t, int align, int extrap, int ignoreLast)
{
	int j, index, N;
	ttNum w_left, w_right=0, t_left, t_right, *p, *p_left, *p_right;
	int sz[N_TRJ][2];
	trjSizes(sz, T->nx, T->nu, T->nf, T->nr, T->nc, T->ndx, T->nuser);

	// ignore last element ?
	N = T->N - ignoreLast;

	if( N<1 )
		tt_error("interpTrajectory: 'trajectory must have at least one element.'");

	// clamp to limits (extrapolate with constants)
	if ( !extrap )
		t = MIN( MAX(t, T->e[0].t[0]), T->e[N-1].t[0]);

	if (align == -1)	// unaligned, find weights
	{
		// initial interval
		t_left 	= T->e[0].t[0];
		t_right = T->e[MIN(1, N-1)].t[0];

		for( index=0; index<N-1; index++ )
		{
			t_left  = T->e[index].t[0];
			t_right = T->e[index+1].t[0];
			if( t < t_left ||  ( t_left <= t && t <= t_right ) || index == N-2)
				break;
		}

		// get weight of left point
		w_left	= ( t_left != t_right ) ? (t_right - t) / (t_right - t_left) : 1;
	}
	else 			// aligned
	{
		index 	= MIN(align, N-1);
		w_left 	= 1;
	}
	w_right = 1 - w_left;

	// interpolate
	for(j=0; j<N_TRJ; j++)
		if ( (p = *(&e->t + j)) && (T->flg & (1<<j)) )
		{
			p_left	= *(&(T->e[index].t)+j);
			tt_scl(p, p_left, w_left, sz[j][0]*sz[j][1]);
			if ( w_right )
			{
				p_right = *(&(T->e[index+1].t)+j);
				tt_addToScl(p, p_right, w_right, sz[j][0]*sz[j][1]);
			}
		}
}

void zeroTrajectory(trajectory *T)
{
	int i, j, zero_outside_range;
	if( T )
	{
		tt_zero(T->buffer+2*T->nu, T->szBuffer-2*T->nu); // don't touch limits
		T->trace.mu		= 1;
		T->trace.dmu	= 1;
		T->trace.cost	= tt_sqrt(-1.0);
		for( i=0; i<T->nu; i++ )
		{
			zero_outside_range =  T->lower && (T->lower[i] < T->upper[i]) && (T->lower[i] > 0 || T->upper[i] < 0);
			for( j=0; j<T->N-1; j++ )
				T->e[j].u[i] = zero_outside_range ? (T->lower[i]+T->upper[i])/2 : 0;
		}
	}
}

// de-allocate trajectory
void deleteTrajectory(trajectory* T)
{
	if( T )
	{
		tt_free(T->buffer);
		tt_free(T->e);
		tt_free(T);
	}
}

int saveTrajectory(const trajectory* T, int szbuf, char* buffer)
{
	int bufadr = 0;

	// standard header: traj size, e size, t_buffer size, TO DO: float/double
	int header[3] = {sizeof(trajectory), sizeof(trajElement)*T->N, T->szBuffer*sizeof(ttNum)};

	if( szbuf<2 )
	{
		printf("traj buffer too small\n");
		return -1;
	}
	memcpy(buffer, header, 3*sizeof(int));
	bufadr+=3*sizeof(int);

	if( szbuf-bufadr<header[0] )
	{
		printf("traj buffer too small 2\n");
		return -1;
	}
	memcpy(buffer+bufadr, T, sizeof(trajectory));
	bufadr+=sizeof(trajectory);

	if( szbuf<T->szBuffer )
	{
		printf("traj buffer too small 3\n");
		return -1;
	}
	memcpy(buffer+bufadr, T->buffer, T->szBuffer*sizeof(ttNum));

	return 3*sizeof(int)+sizeof(trajectory)+T->szBuffer*sizeof(ttNum);
}


trajectory* loadTrajectory(trajectory* dest, int szbuf, void* buffer)
{
	int header[3];
	int N;
	int bufadr=0;
	ttNum* p_trj_b;
	trajElement *p_trj_e;
	trajectory* out;
	if( szbuf-bufadr<3*sizeof(int) )
	{
		printf("traj buffer too small 0\n");
		return 0;
	}
	memcpy(header, buffer, 3*sizeof(int));
	bufadr+=3*sizeof(int);
	if( header[0]!=sizeof(trajectory) )
	{
		printf("buffer corrupted 0\n");
		return 0;
	}
	N=header[1]/sizeof(trajElement);

	if( dest )
	{
		if( dest->N!=N || header[2]!=dest->szBuffer*sizeof(ttNum) )
		{
			printf("dest mismatch\n");
			return 0;
		}
		out=dest;
		p_trj_b=out->buffer;
		p_trj_e=out->e;
	}
	else
	{
		// to do: add contents to info etc.
		//	out=newTrajectory(...
		printf("load into new traj not supported yet\n");
		return 0;
	}

	if( szbuf-bufadr<header[0] )
	{
		printf("traj buffer too small 1\n");
		return 0;
	}
	memcpy(out, (char*)buffer+bufadr, header[0]);
	bufadr+=header[0];
	// fix pointers:
	out->buffer=p_trj_b;
	out->e=p_trj_e;

	if( N!=out->N || header[2]!=out->szBuffer*sizeof(ttNum) )
	{
		printf("traj misinterpreted\n");
		return 0;
	}

	if( szbuf-bufadr<header[2] )
	{
		printf("traj buffer too small 3\n");
		return 0;
	}
	memcpy(out->buffer, (char*)buffer+bufadr, header[2]);

	setPtrTrajectory(out);

	return out;
}



