#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "float.h"
#include "tt_util.h"
#include <stdarg.h>

#ifdef ttUSEAVX
	#ifdef _NIX
		#include <immintrin.h>
	#else
		#include <intrin.h>
	#endif
#endif

#if defined(ttMKL_MUL) || defined(ttMKL_CHOL)
	#include "mkl.h"
	#include "mkl_blas.h"
	#include "mkl_lapack.h"
#endif

#ifdef mjUSEIPP
	#include "ipp.h"
#endif

#ifdef _MEX
	#include "mj_engine.h"
	#include "mex.h"
#endif

// allocate size ttNums on scratch
ttNum* tt_stackAlloc(ttNum **scratch, int *szScratch, int size)
{
	ttNum* result;

	// check size
	if( *szScratch < size )
		tt_error("stack overflow");

	result = (ttNum*)*scratch;
	*szScratch -= size;
	*scratch += size;
	return result;
}

//------------------------------ error handling --------------------------------------

// write message to logfile and console, pause and exit
void tt_error(const char* msg)
{
#ifndef _MEX
	printf("TT ERROR: %s\n\nPress Enter to exit ...", msg);

	// pause, exit
	getchar();
	exit(1);
#else
	mexErrMsgTxt(msg);
#endif
}


// yuval: did you know you can have variable argument lists in C?
void tt_warning_new( const char * msg, ... )
{
  va_list args;
  va_start (args, msg);
  vprintf (msg, args);
  va_end (args);
}


// write message to logfile and console
void tt_warning(const char* msg)
{
#ifndef _MEX
	printf("TT WARNING: %s\n\n", msg);
#else
	mexPrintf("TT WARNING: %s\n\n", msg);
#endif
}


// error with int argument
void tt_error_i(const char* msg, int i)
{
	char errmsg[300];
	sprintf(errmsg, msg, i);
	tt_error(errmsg);
}


// warning with int argument
void tt_warning_i(const char* msg, int i)
{
	char wrnmsg[300];
	sprintf(wrnmsg, msg, i);
	tt_warning(wrnmsg);
}


// error string argument
void tt_error_s(const char* msg, const char* text)
{
	char errmsg[300];
	sprintf(errmsg, msg, text);
	tt_error(errmsg);
}


// warning string argument
void tt_warning_s(const char* msg, const char* text)
{
	char wrnmsg[300];
	sprintf(wrnmsg, msg, text);
	tt_warning(wrnmsg);
}


// write datetime, type: message to MUJOCO_LOG.TXT
void tt_write_log(const char* type, const char* msg)
{
	time_t rawtime;
	struct tm *timeinfo;
	FILE* fp = fopen("MPC_LOG.TXT", "a+t");
	if( fp )
	{
		// get time
		time( &rawtime );
		timeinfo = localtime( &rawtime );

		// write to log file
		fprintf(fp, "%s%s: %s\n\n", asctime(timeinfo), type, msg);
		fclose(fp);
	}
}



//------------------------------ malloc and free ------------------------------------

// allocate memory, align
void* tt_malloc(unsigned int sz, unsigned int align)
{
#ifndef _MEX
	void* ptr = 0;

	// try to allocate
	#ifdef _NIX
	   // could use a check for if "sz" is a multiple of "align"
		if (align == 8 || sz % align) {
			ptr = malloc(sz); // malloc is always 8 aligned
		}
		else {
			//ptr = aligned_alloc(align, sz+align);
			ptr = malloc(sz+align);
			while( (unsigned long long)ptr % align)ptr++;
		}
	#else
		ptr = _aligned_malloc(sz, align);
	#endif

	// error if null pointer
	if( !ptr ) {
		tt_error("could not allocate memory");
   }

	return ptr;
#else
	return mju_malloc(sz, align);
#endif
}


// free memory
void tt_free(void* ptr)
{
#ifndef _MEX
	#ifdef _NIX
		free(ptr);
	#else
		_aligned_free(ptr);
	#endif
#else
	mju_free(ptr);
#endif
}


void tt_copyDF(ttNum *dest, const float *source, int n)
{
	int i;
	for( i=0; i<n; i++ )
		dest[i] = source[i];
}

void tt_copyFD(float *dest, const ttNum *source, int n)
{
	int i;
	for( i=0; i<n; i++ )
		dest[i] = (float)source[i];
}

//------------------------------ 3D vector and matrix-vector operations --------------

// set vector to zero
void tt_zero3(ttNum* res)
{
	res[0] = res[1] = res[2] = 0.0;
}


// copy vector
void tt_copy3(ttNum* res, const ttNum* data)
{
	res[0] = data[0];
	res[1] = data[1];
	res[2] = data[2];
}



// scale vector
void tt_scl3(ttNum* res, const ttNum* vec, ttNum scl)
{
	res[0] = vec[0] * scl;
	res[1] = vec[1] * scl;
	res[2] = vec[2] * scl;
}



// add vectors
void tt_add3(ttNum* res, const ttNum* vec1, const ttNum* vec2)
{
	res[0] = vec1[0] + vec2[0];
	res[1] = vec1[1] + vec2[1];
	res[2] = vec1[2] + vec2[2];
}



// subtract vectors
void tt_sub3(ttNum* res, const ttNum* vec1, const ttNum* vec2)
{
	res[0] = vec1[0] - vec2[0];
	res[1] = vec1[1] - vec2[1];
	res[2] = vec1[2] - vec2[2];
}



// add to vector
void tt_addTo3(ttNum* res, const ttNum* vec)
{
	res[0] += vec[0];
	res[1] += vec[1];
	res[2] += vec[2];
}



// add scaled to vector
void tt_addToScl3(ttNum* res, const ttNum* vec, ttNum scl)
{
	res[0] += vec[0] * scl;
	res[1] += vec[1] * scl;
	res[2] += vec[2] * scl;
}



// normalize vector, return length before normalization
ttNum tt_normalize3(ttNum* vec)
{
	ttNum norm = tt_sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
	ttNum normInv;

	if( norm < ttMINVAL )
	{
		vec[0] = 1.0;
		vec[1] = vec[2] = 0.0;
	}
	else
	{
		normInv = 1.0/norm;
		vec[0] *= normInv;
		vec[1] *= normInv;
		vec[2] *= normInv;
	}

	return norm;
}



// compute vector length (without normalizing)
ttNum tt_norm3(const ttNum* vec)
{
	return tt_sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
}



// vector dot-product
ttNum tt_dot3(const ttNum* vec1, const ttNum* vec2)
{
	return vec1[0]*vec2[0] + vec1[1]*vec2[1] + vec1[2]*vec2[2];
}



// Cartesian distance between 3D vectors
ttNum tt_dist3(const ttNum* pos1, const ttNum* pos2)
{
	ttNum dif[3] = {pos1[0]-pos2[0], pos1[1]-pos2[1], pos1[2]-pos2[2]};
	return tt_sqrt(dif[0]*dif[0] + dif[1]*dif[1] + dif[2]*dif[2]);
}



// multiply vector by 3D rotation matrix
void tt_rotVecMat(ttNum* res, const ttNum* vec, const ttNum* mat)
{
	res[0] = mat[0]*vec[0] + mat[1]*vec[1] + mat[2]*vec[2];
	res[1] = mat[3]*vec[0] + mat[4]*vec[1] + mat[5]*vec[2];
	res[2] = mat[6]*vec[0] + mat[7]*vec[1] + mat[8]*vec[2];
}



// multiply vector by transposed 3D rotation matrix
void tt_rotVecMatT(ttNum* res, const ttNum* vec, const ttNum* mat)
{
	res[0] = mat[0]*vec[0] + mat[3]*vec[1] + mat[6]*vec[2];
	res[1] = mat[1]*vec[0] + mat[4]*vec[1] + mat[7]*vec[2];
	res[2] = mat[2]*vec[0] + mat[5]*vec[1] + mat[8]*vec[2];
}



//------------------------------ vector operations ---------------------------------

// set vector to zero
void tt_zero(ttNum* res, int n)
{
	memset(res, 0, n*sizeof(ttNum));
}



// set matrix to diagonal
void tt_diag(ttNum* mat, ttNum value, int n)
{
	int i;
	tt_zero(mat, n*n);
	for( i=0; i<n; i++ )
		mat[i*(n+1)] = value;
}


// copy vector
void tt_copy(ttNum* res, const ttNum* vec, int n)
{
	memcpy(res, vec, n*sizeof(ttNum));
}


// move vector
void tt_move(ttNum* res, const ttNum* vec, int n)
{
	memmove(res, vec, n*sizeof(ttNum));
}


// scale vector
void tt_scl(ttNum* res, const ttNum* vec, ttNum scl, int n)
{
	int i;
	for( i=0; i<n; i++ )
		res[i] = vec[i] * scl;
}


// fill buffer with value
void tt_fill(ttNum* vec, const ttNum val, const int n)
{	int i;
	for(i=0; i<n; i++)
		vec[i] = val;
}


// sum vector
ttNum tt_sum(const ttNum* vec, int n)
{
	int i;
	ttNum res=0;
	for( i=0; i<n; i++ )
		res += vec[i];
	return res;
}


// add vectors
void tt_add(ttNum* res, const ttNum* vec1, const ttNum* vec2, int n)
{
	int i;
	for( i=0; i<n; i++ )
		res[i] = vec1[i] + vec2[i];
}


// subtract vectors
void tt_sub(ttNum* res, const ttNum* vec1, const ttNum* vec2, int n)
{
	int i;
	for( i=0; i<n; i++ )
		res[i] = vec1[i] - vec2[i];
}


// add to vector
void tt_addTo(ttNum* res, const ttNum* vec, int n)
{
	int i;
	for( i=0; i<n; i++ )
		res[i] += vec[i];
}


// add scaled to vector... FMA ???
void tt_addToScl(ttNum* res, const ttNum* vec, ttNum scl, int n)
{
	int i = 0;

// no AVX
#if !defined(ttUSEAVX)

	for( i=0; i<n; i++ )
		res[i] += vec[i]*scl;

// AVX
#else

// double
#if defined(ttUSEDOUBLE)
	__m256d V, R, S;
	if( n>=4 )
	{
		S = _mm256_broadcast_sd(&scl);
		for( i=0; i<=n-4; i+=4 )
		{
			V = _mm256_loadu_pd(vec+i);
			R = _mm256_loadu_pd(res+i);
			V = _mm256_mul_pd(V, S);
			R = _mm256_add_pd(R, V);
			_mm256_storeu_pd(res+i, R);
		}
	}

// single
#else
	__m256 V, R, S;
	if( n>=8 )
	{
		S = _mm256_broadcast_ss(&scl);
		for( i=0; i<=n-8; i+=8 )
		{
			V = _mm256_loadu_ps(vec+i);
			R = _mm256_loadu_ps(res+i);
			V = _mm256_mul_ps(V, S);
			R = _mm256_add_ps(R, V);
			_mm256_storeu_ps(res+i, R);
		}
	}
#endif

	// finalize
	while( i<n )
	{
		res[i] += vec[i]*scl;
		i++;
	}
#endif
}



// normalize vector, return length before normalization
ttNum tt_normalize(ttNum* res, int n)
{
	int i;
	ttNum norm = (ttNum)tt_sqrt(tt_dot(res, res, n));
	ttNum normInv;

	if( norm < ttMINVAL )
	{
		res[0] = 1;
		for( i=1; i<n; i++ )
			res[i] = 0;
	}
	else
	{
		normInv = 1.0/norm;
		for( i=0; i<n; i++ )
			res[i] *= normInv;
	}

	return norm;
}



// compute vector length (without normalizing)
ttNum tt_norm(const ttNum* res, int n)
{
	return tt_sqrt(tt_dot(res, res, n));
}



// vector dot-product... FMA ???
ttNum tt_dot(const ttNum* vec1, const ttNum* vec2, const int n)
{
	int i = 0;
	ttNum res = 0;

#if defined(ttUSEAVX)

	// double
	#if defined(ttUSEDOUBLE)
	ttNum store[4];
	__m256d tmp;

		if( n>=4 )
		{
		tmp = _mm256_setzero_pd();

			for( i=0; i<=n-4; i+=4 )
				tmp = _mm256_add_pd(tmp, _mm256_mul_pd(
				_mm256_loadu_pd(vec1+i), _mm256_loadu_pd(vec2+i)));

			_mm256_storeu_pd(store, tmp);
			res = store[0] + store[1] + store[2] + store[3];
		}

	// single
	#else
	ttNum store[8];
	__m256 tmp;

		if( n>=8 )
		{
		tmp = _mm256_setzero_ps();

			for( i=0; i<=n-8; i+=8 )
				tmp = _mm256_add_ps(tmp, _mm256_mul_ps(
				_mm256_loadu_ps(vec1+i), _mm256_loadu_ps(vec2+i)));

			_mm256_storeu_ps(store, tmp);
			res = store[0] + store[1] + store[2] + store[3] +
				+ store[4] + store[5] + store[6] + store[7];
		}
	#endif

	// finalize
	while( i<n )
	{
		res += vec1[i]*vec2[i];
		i++;
	}

#elif defined(ttUSEIPP)

	ippsDotProd_64f(vec1,vec2,n,&res);

#else
	for( i=0; i<n; i++ )
		res += vec1[i] * vec2[i];
#endif

return res;
}



//------------------------------ matrix-vector operations ----------------------------

// multiply matrix and vector
void tt_mulMatVec(ttNum* res, const ttNum* mat, const ttNum* vec,
				   int nr, int nc)
{
	int r;
	for( r=0; r<nr; r++ )
	   res[r] = tt_dot(mat + r*nc, vec, nc);
}



// multiply transposed matrix and vector
void tt_mulMatTVec(ttNum* res, const ttNum* mat, const ttNum* vec,
					int nr, int nc)
{
   int r;
   ttNum tmp;
   tt_zero(res,nc);

   for( r=0; r<nr; r++ )
	  if (tmp = vec[r])
		 tt_addToScl(res, mat+r*nc, tmp, nc);
}





//------------------------------ matrix-matrix operations ----------------------------


// identity matrix
void tt_eye(ttNum* mat, int n)
{
	int i;
	tt_zero(mat, n*n);
	for (i=0; i<n; i++)
		mat[i*n+i] = 1.0;
}


// transpose matrix
void tt_transpose(ttNum* res, const ttNum* mat, int r, int c)
{
	int i, j;
	ttNum temp;

	if( res!=mat )
		for( i=0; i<r; i++ )
			for( j=0; j<c; j++ )
				res[j*r+i] = mat[i*c+j];
	else if ( r==c )
		for( i=0; i<r; i++ )
			for( j=0; j<i; j++ )
			{   // in-place swap
				temp		= res[j*r+i];
				res[j*r+i]	= res[i*r+j];
				res[i*r+j]	= temp;
			}
	else
		tt_error("tt_transpose - only square matrices can be transposed in-place.");
}



// enforce symmetry
void tt_symmetrize(ttNum* mat, int n)
{
	int i, j;
	ttNum tmp;

   for( i=0; i<n; i++ )
	  for( j=0; j<i; j++ )
	  {
		 tmp = 0.5*(mat[i*n+j] + mat[j*n+i]);
		 mat[i*n+j] = mat[j*n+i] = tmp;
	  }
}



// multiply matrices, exploit sparsity of mat1
void tt_mulMatMat(ttNum* res, const ttNum* mat1, const ttNum* mat2,
				   int r1, int c1, int c2)
{
#if !defined(ttMKL_MUL)
   int i, k;
   ttNum tmp;

   tt_zero(res, r1*c2);

   for( i=0; i<r1; i++ )
	  for( k=0; k<c1; k++ )
		 if( (tmp = mat1[i*c1+k]) )
			tt_addToScl(res+i*c2, mat2+k*c2, tmp, c2);

#else
	#if defined(ttUSEDOUBLE)
		const int m=c2, n=r1 ,p=c1;
		double one = 1.0, zero = 0.0;
		dgemm("N", "N", &m, &n, &p, &one, mat2, &m, mat1, &p, &zero, res, &m);
	#else
		size_t m=c2, n=r1 ,p=c1;
		float one = 1.0, zero = 0.0;
		sgemm("N", "N", &m, &n, &p, &one, mat2, &m, mat1, &p, &zero, res, &m);
	#endif
#endif
}



// multiply matrices, second argument transposed
void tt_mulMatMatT(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					int r1, int c1, int r2)
{
#if !defined(ttMKL_MUL)
	int i, j;

	for( i=0; i<r1; i++ )
		for( j=0; j<r2; j++ )
			res[i*r2+j] = tt_dot(mat1+i*c1, mat2+j*c1, c1);
#else
	#if defined(ttUSEDOUBLE)
		const int m=r2, n=r1 ,p=c1;
		double one = 1.0, zero = 0.0;
		dgemm("T", "N", &m, &n, &p, &one, mat2, &p, mat1, &p, &zero, res, &m);
	#else
		size_t m=r2, n=r1 ,p=c1;
		double one = 1.0, zero = 0.0;
		sgemm("T", "N", &m, &n, &p, &one, mat2, &p, mat1, &p, &zero, res, &m);
	#endif
#endif
}



// compute M*M'
void tt_sqrMat(ttNum* res, const ttNum* mat, int r, int c,
				ttNum* scratch, int szScratch)
{
	int i, j;

// no BLAS
#if !defined(ttMKL_MUL)
	int k;
	ttNum tmp;

	// make transposed in scratch
	if( szScratch < r*c )
		tt_error("tt_sqrMat - insufficient scratch space");	// SHOULD NOT OCCUR
	tt_transpose(scratch, mat, r, c);

	// half of MatMat routine: only lower triangle
	tt_zero(res, r*r);
	for( i=0; i<r; i++ )
		for( k=0; k<c; k++ )
			if( (tmp = mat[i*c+k]) )
				tt_addToScl(res+i*r, scratch+k*r, tmp, i+1);

// BLAS
#else

#if defined(ttUSEDOUBLE)
	const double one = 1.0, zero = 0.0;
	dsyrk("U", "T", &r, &c, &one, mat, &c, &zero, res, &r);
#else
	const float one = 1.0, zero = 0.0;
	ssyrk("U", "T", &r, &c, &one, mat, &c, &zero, res, &r);
#endif

#endif

	// make symmetric
	for( i=0; i<r; i++ )
		for( j=i+1; j<r; j++ )
			res[i*r+j] = res[j*r+i];
}


// compute M'*M
void tt_sqrMatT(ttNum* res, const ttNum* mat, int r, int c)
{
	int i, j;
// no BLAS
#if !defined(ttMKL_MUL)
	int k;
	ttNum tmp;

	// half of MatMat routine: only lower triangle
	tt_zero(res, c*c);
	for( k=0; k<r; k++ )
		for( i=0; i<c; i++ )
			if( (tmp = mat[k*c+i]) )
				tt_addToScl(res+i*c, mat+k*c, tmp, i+1);

// BLAS
#else

#if defined(ttUSEDOUBLE)
	const double one = 1.0, zero = 0.0;
	dsyrk("U", "N", &c, &r, &one, mat, &c, &zero, res, &c);
#else
	const float one = 1.0, zero = 0.0;
	ssyrk("U", "N", &c, &r, &one, mat, &c, &zero, res, &c);
#endif

#endif

	// make symmetric
	for( i=0; i<c; i++ )
		for( j=i+1; j<c; j++ )
			res[i*c+j] = res[j*c+i];

}


// compute M1*M2 + M2'*M1'
void tt_sqrMatMat(ttNum* res, const ttNum* mat1, const ttNum* mat2, int r, int c,
				   ttNum* scratch, int szScratch)
{
// no BLAS
#if !defined(ttMKL_MUL)
	if( szScratch < c*c )
		tt_error("tt_sqrMatMat - insufficient scratch space");    // SHOULD NOT OCCUR

	tt_mulMatMat(res, mat1, mat2, r, c, r);
	tt_transpose(scratch, res, r, r);
	tt_addTo(res, scratch, r*r);

// BLAS
#else
	int i,j;
	const ttNum one = 1.0, zero = 0.0;
	const int _r=r, _c=c;

	if( szScratch < r*c )
		tt_error("tt_sqrMatMat - insufficient scratch space");    // SHOULD NOT OCCUR

	tt_transpose(scratch, mat1, r, c);

	#if defined(ttUSEDOUBLE)
		dsyr2k("U", "N", &_r, &_c, &one, scratch, &_r, mat2, &_r, &zero, res, &_r);
	#else
		ssyr2k("U", "N", &r, &c, &one, scratch, &r, mat2, &r, &zero, res, &r);
	#endif

	// make symmetric
	for( i=0; i<r; i++ )
		for( j=i+1; j<r; j++ )
			res[i*r+j] = res[j*r+i];
#endif
}




// multiply matrices, first argument transposed
void tt_mulMatTMat(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					int r1, int c1, int c2)
{
#if !defined(ttMKL_MUL)
	int i, j;
	ttNum tmp;

   tt_zero(res, c1*c2);

	for( i=0; i<r1; i++ )
		for( j=0; j<c1; j++ )
		 if ( (tmp = mat1[i*c1+j]) )
			tt_addToScl(res+j*c2, mat2+i*c2, tmp, c2);

#else
	#if defined(ttUSEDOUBLE)
		const int m=c2, n=c1 ,p=r1;
		double one = 1.0, zero = 0.0;
		dgemm("N", "T", &m, &n, &p, &one, mat2, &m, mat1, &n, &zero, res, &m);
	#else
		size_t m=c2, n=c1 ,p=r1;
		float one = 1.0, zero = 0.0;
		sgemm("N", "T", &m, &n, &p, &one, mat2, &m, mat1, &n, &zero, res, &m);
	#endif
#endif
}



// multiply matrices, sparse
void tt_mulMatMat_S(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					 int r1, int c1, int c2, ttNum* scratch, int szScratch)
{
	int i, j, k;
	int nz, *ind = (int*)scratch;
	ttNum tmp;

	// check space
	if( szScratch*sizeof(ttNum) < r1*sizeof(int) )
		tt_error("tt_mulMatMat_S - insufficient scratch space");	// SHOULD NOT OCCUR

	// clear result matrix
	tt_zero(res, r1*c2);

	// loop over common dimension
	for( k=0; k<c1; k++ )
	{
		// find indices of non-zeros in column k of mat1
		nz = 0;
		for( i=0; i<r1; i++ )
			if( mat1[i*c1+k] )
				ind[nz++] = i;

		// find non-zeros in row k of mat2, multiply by column k of mat1
		for( j=0; j<c2; j++ )
			if( (tmp=mat2[k*c2+j]) )
				for( i=0; i<nz; i++ )
					res[ind[i]*c2+j] += tmp * mat1[ind[i]*c1+k];
	}
}



// multiply matrices, second argument transposed, sparse
void tt_mulMatMatT_S(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					  int r1, int c1, int r2, ttNum* scratch, int szScratch)
{
	int i, j, k;
	int nz, *ind = (int*)scratch;
	ttNum tmp;

	// check space
	if( szScratch*sizeof(ttNum) < r1*sizeof(int) )
		tt_error("tt_mulMatMatT_S - insufficient scratch space");	// SHOULD NOT OCCUR

	// clear result matrix
	tt_zero(res, r1*r2);

	// loop over common dimension
	for( k=0; k<c1; k++ )
	{
		// find indices of non-zeros in column k of mat1
		nz = 0;
		for( i=0; i<r1; i++ )
			if( mat1[i*c1+k] )
				ind[nz++] = i;

		// find non-zeros in row k of mat2', multiply by column k of mat1
		for( j=0; j<r2; j++ )
			if( (tmp=mat2[j*c1+k]) )
				for( i=0; i<nz; i++ )
					res[ind[i]*r2+j] += tmp * mat1[ind[i]*c1+k];
	}
}



// sparse symmetric matrix multiplication, second argument transposed
void tt_mulMatMatT_SS(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					   int r, int c, ttNum* scratch, int szScratch)
{
	int i, j, k;
	int nz, *ind = (int*)scratch;
	ttNum tmp;

	// check space
	if( szScratch*sizeof(ttNum) < r*sizeof(int) )
		tt_error("tt_mulMatMatT_SS - insufficient scratch space");	// SHOULD NOT OCCUR

	// clear result matrix
	tt_zero(res, r*r);

	// loop over common dimension
	for( k=0; k<c; k++ )
	{
		// find indices of non-zeros in column k of mat1
		nz = 0;
		for( i=0; i<r; i++ )
			if( mat1[i*c+k] )
				ind[nz++] = i;

		// find non-zeros in row k of mat2', multiply by column k of mat1
		for( j=0; j<r; j++ )
			if( (tmp=mat2[j*c+k]) )
				for( i=0; i<nz && ind[i]<=j; i++ )
					res[ind[i]*r+j] += tmp * mat1[ind[i]*c+k];
	}

	// make symmetric
	for( i=1; i<r; i++ )
		for( j=0; j<i; j++ )
			res[i*r+j] = res[j*r+i];
}



// sparse symmetric matrix multiplication, first argument transposed
void tt_mulMatTMat_SS(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					   int r, int c, ttNum* scratch, int szScratch)
{
	int i, j, k;
	int nz, *ind = (int*)scratch;
	ttNum tmp;

	// check space
	if( szScratch*sizeof(ttNum) < c*sizeof(int) )
		tt_error("tt_mulMatTMat_SS - insufficient scratch space");	// SHOULD NOT OCCUR

	// clear result matrix
	tt_zero(res, r*r);

	// loop over common dimension
	for( k=0; k<r; k++ )
	{
		// find indices of non-zeros in column k of mat1'
		nz = 0;
		for( i=0; i<c; i++ )
			if( mat1[k*c+i] )
				ind[nz++] = i;

		// find non-zeros in row k of mat2, multiply by column k of mat1'
		for( j=0; j<r; j++ )
			if( (tmp=mat2[k*c+j]) )
				for( i=0; i<nz && ind[i]<=j; i++ )
					res[ind[i]*c+j] += tmp * mat1[k*c+ind[i]];
	}

	// make symmetric
	for( i=1; i<c; i++ )
		for( j=0; j<i; j++ )
			res[i*c+j] = res[j*c+i];
}



// rank-one update to symmetric matrix, enforce symmetry
void tt_rankOneUpdateSym(ttNum* res, const ttNum* vec1, const ttNum* vec2,
						  ttNum scl, int n)
{
	int r, c;
	ttNum tmp;

	for( r=0; r<n; r++ )
		for( c=r; c<n; c++ )
		{
			// symmetrized outer product
			tmp = scl*(vec1[r]*vec2[c] + vec1[c]*vec2[r])/2;

			// add to matrix
			res[r*n+c] += tmp;
			if( r!=c )
				res[c*n+r] += tmp;
		}
}



//------------------------------ tensor operations ----------------------------

// multiply vector by tensor, output matrix
void tt_multVecTens(ttNum* res, const ttNum* vec, const ttNum* tens,
				   const int r, const int c, const int p)
{
	int i, j, k;
	ttNum tmp;

   for( i=0; i<r; i++ )
		if (tmp = vec[i])
			for( j=0; j<p; j++ )
				for( k=0; k<c; k++ )
					res[k*p+j] += tmp * tens[r*c*j + c*i + k];

}



// multiply vector by symmetric tensor, output symmetric matrix
void tt_multVecSymTens(ttNum* res, const ttNum* vec, const ttNum* tens,
				   const int r, const int c)
{
	int i, j;
	ttNum tmp;

   for( i=0; i<r; i++ )
	  for( j=0; j<c; j++ )
			if (tmp = vec[i])
				tt_addToScl(res + j*c, tens + r*c*j + c*i, tmp, j+1);

   // symmetrize
   for( j=0; j<c; j++ )
	  for( i=0; i<j; i++ )
		 res[i*c+j] = res[j*c+i];
}



//------------------------------ quaternion operations -----------------------------

// rotate vector by quaternion
void tt_rotVecQuat(ttNum* res, const ttNum* vec, const ttNum* quat)
{
	ttNum mat[9];
	tt_quat2Mat(mat, quat);
	tt_rotVecMat(res, vec, mat);
}



// muiltiply quaternions
void tt_mulQuat(ttNum* res, const ttNum* qa, const ttNum* qb)
{
	res[0] = qa[0]*qb[0] - qa[1]*qb[1] - qa[2]*qb[2] - qa[3]*qb[3];
	res[1] = qa[0]*qb[1] + qa[1]*qb[0] + qa[2]*qb[3] - qa[3]*qb[2];
	res[2] = qa[0]*qb[2] - qa[1]*qb[3] + qa[2]*qb[0] + qa[3]*qb[1];
	res[3] = qa[0]*qb[3] + qa[1]*qb[2] - qa[2]*qb[1] + qa[3]*qb[0];
}



// negate quaternion
void tt_negQuat(ttNum* res, const ttNum* quat)
{
	res[0] = quat[0];
	res[1] = -quat[1];
	res[2] = -quat[2];
	res[3] = -quat[3];
}



// convert axisAngle to quaternion
void tt_axisAngle2Quat(ttNum* res, const ttNum* axis, ttNum angle)
{
	ttNum s = tt_sin(angle*0.5);
	res[0] = tt_cos(angle*0.5);
	res[1] = axis[0]*s;
	res[2] = axis[1]*s;
	res[3] = axis[2]*s;
}



// convert quaternion (corresponding to orientation difference) to 3D velocity
void tt_quat2Vel(ttNum* res, const ttNum* quat, ttNum dt)
{
	ttNum axis[3] = {quat[1], quat[2], quat[3]};
	ttNum sin_a_2 = tt_normalize3(axis);
	ttNum speed = 2 * tt_atan2(sin_a_2, quat[0]) / dt;

	tt_scl3(res, axis, speed);
}



// convert quaternion to 3D rotation matrix
void tt_quat2Mat(ttNum* res, const ttNum* quat)
{
	ttNum q00 = quat[0]*quat[0];
	ttNum q11 = quat[1]*quat[1];
	ttNum q22 = quat[2]*quat[2];
	ttNum q33 = quat[3]*quat[3];
	res[0] = q00 + q11 - q22 - q33;
	res[4] = q00 - q11 + q22 - q33;
	res[8] = q00 - q11 - q22 + q33;
	res[1] = 2*(quat[1]*quat[2] - quat[0]*quat[3]);
	res[2] = 2*(quat[1]*quat[3] + quat[0]*quat[2]);
	res[3] = 2*(quat[1]*quat[2] + quat[0]*quat[3]);
	res[5] = 2*(quat[2]*quat[3] - quat[0]*quat[1]);
	res[6] = 2*(quat[1]*quat[3] - quat[0]*quat[2]);
	res[7] = 2*(quat[2]*quat[3] + quat[0]*quat[1]);
}



// convert 3D rotation matrix to quaterion... reorder axes if trace is negative ???
void tt_mat2Quat(ttNum* quat, const ttNum* mat)
{
	ttNum trace = 1.0 + mat[0] + mat[4] + mat[8];

	if( trace < ttMINVAL )		// SHOULD NOT OCCUR
	{
		tt_error("negative trace in tt_mat2Quat");
		quat[0] = 1;
		quat[1] = quat[2] = quat[3] = 0;
		return;
	}

	// compute quaternion
	quat[0] = sqrt(trace) / 2.0;
	quat[1] = (mat[7] - mat[5]) / quat[0] / 4.0;
	quat[2] = (mat[2] - mat[6]) / quat[0] / 4.0;
	quat[3] = (mat[3] - mat[1]) / quat[0] / 4.0;
	tt_normalize(quat, 4);
}



// time-derivative of quaternion, given 3D rotational velocity
void tt_derivQuat(ttNum* res, const ttNum* quat, const ttNum* vel)
{
	res[0] = 0.5*(-vel[0]*quat[1] - vel[1]*quat[2] - vel[2]*quat[3]);
	res[1] = 0.5*( vel[0]*quat[0] + vel[1]*quat[3] - vel[2]*quat[2]);
	res[2] = 0.5*(-vel[0]*quat[3] + vel[1]*quat[0] + vel[2]*quat[1]);
	res[3] = 0.5*( vel[0]*quat[2] - vel[1]*quat[1] + vel[2]*quat[0]);
}



// integrate quaterion given 3D angular velocity
void tt_quatIntegrate(ttNum* quat, const ttNum* vel, ttNum scale)
{
	ttNum angle, tmp[4], qrot[4];

	// form local rotation quaternion, apply
	tt_copy3(tmp, vel);
	angle = scale * tt_normalize3(tmp);
	tt_axisAngle2Quat(qrot, tmp, angle);
	tt_mulQuat(tmp, quat, qrot);
	tt_normalize(tmp, 4);
	tt_copy(quat, tmp, 4);
}



// compute quaternion performing rotation from given vector to z-axis
void tt_quatZ2Vec(ttNum* quat, const ttNum* vec)
{
	ttNum axis[3], a, vn[3] = {vec[0],vec[1],vec[2]}, z[3] = {0,0,1};

	// set default result to no-rotation quaternion
	quat[0] = 1;
	tt_zero3(quat+1);

	// normalize vector; if too small, no rotation
	if( tt_normalize3(vn)<ttMINVAL )
		return;

	// compute angle and axis
	tt_cross(axis, z, vn);
	a = tt_normalize3(axis);

	// almost parallel
	if( fabs(a)<ttMINVAL )
	{
		// opposite: 180 deg rotation around x axis
		if( tt_dot3(vn, z) < 0 )
		{
			quat[0] = 0;
			quat[1] = 1;
		}

		return;
	}

	// make quaterion from angle and axis
	a = tt_atan2(a, tt_dot3(vn, z));
	tt_axisAngle2Quat(quat, axis, a);
}



// Cholesky decomposition
int tt_cholFactor(ttNum* mat, ttNum* diag, int n,
				   ttNum minabs, ttNum minrel, ttNum* correct)
{
int rank = n;
// no BLAS
#if !defined(ttMKL_CHOL)
	int i, j;
	ttNum tmp;

	// clear corrections
	if( correct )
		tt_zero(correct, n);

	// compute relative minimum if needed
	if( minrel>0 )
	{
		tmp = 0;
		for( i=0; i<n; i++ )
			tmp += mat[i*(n+1)];
		minabs = tt_max(minabs, minrel*tmp/n);
	}

	// smallest allowed minimum is ttMINVAL
	minabs = tt_max(minabs, ttMINVAL);

	// in-place Cholesky factorization, use diag to store 1/L(j,j)
	for( j=0; j<n; j++ )
	{
		tmp = mat[j*n+j];
		if( j )
			tmp -= tt_dot(mat+j*n, mat+j*n, j);

		// handle diagonal values below threshold
		if( tmp < minabs )
		{
			// save correction
			if( correct )
				correct[j] = minabs - tmp;
			tmp = minabs;
			rank--;
		}

		// save square root of diagonal, to speed up back-substitution
		diag[j] = (ttNum)(1.0/tt_sqrt(tmp));

		// process off-diagonal entries, modify 'mat'
		tmp = diag[j];
		for( i=j+1; i<n; i++ )
			mat[i*n+j] = (mat[i*n+j] - tt_dot(mat+i*n, mat+j*n, j)) * tmp;
	}

	// copy diag to diagonal of mat
	for( j=0; j<n; j++ )
		mat[j*n+j] = diag[j];

// BLAS
#else
	const int _n=n;
	int info;

	#if defined(ttUSEDOUBLE)
		dpotrf( "U", &_n, mat, &_n, &info);
	#else
		spotrf( "U", &_n, mat, &_n, &info);
	#endif

	rank = MAX( n-(int)info, 0);
#endif

	return rank;
}



// Cholesky backsubstitution
void tt_cholBacksub(ttNum* res, const ttNum* mat, const ttNum* vec,
					 int n, int nvec, ttByte half)
{
// no BLAS
#if !defined(ttMKL_CHOL)
	int i, j, ivec;
	ttNum* res_p;

	// copy if source and destination are different
	if( res!=vec )
		tt_copy(res, vec, n*nvec);

	// solve for multiple vectors via backsubstitution
	for( ivec=0; ivec<nvec; ivec++ )
	{
		// precompute pointer to corresponding vector in res
		res_p = res+n*ivec;

		// forward substitution: solve L*res = vec
		for( i=0; i<n; i++ )
		{
			if( i )
				res_p[i] -= tt_dot(mat+i*n, res_p, i);

			res_p[i] *= mat[i*n+i];
		}

		// backward substitution: solve L'*res = res
		if( !half )
			for( i=n-1; i>=0; i-- )
			{
				if( i < n-1 )
					for( j=i+1; j<n; j++ )
						res_p[i] -= mat[j*n+i] * res_p[j];

				res_p[i] *= mat[i*n+i];
			}
	}

// BLAS
#else
	const int _n=n, _nvec=nvec;
	ttNum one = 1.0;

	// copy if source and destination are different
	if( res!=vec )
		tt_copy(res, vec, n*nvec);

	#if defined(ttUSEDOUBLE)
		dtrsm("L","U","T","N", &_n, &_nvec, &one, mat, &_n, res, &_n);
		if( !half )
			dtrsm("L","U","N","N", &_n, &_nvec, &one, mat, &_n, res, &_n);
	#else
		strsm("L","U","T","N", &_n, &_nvec, &one, mat, &_n, res, &_n);
		if( !half )
			strsm("L","U","N","N", &_n, &_nvec, &one, mat, &_n, res, &_n);
	#endif
#endif
}




// band-diagonal (plus dense) Cholesky decomposition
int tt_bandFactor(ttNum* mat, int nTotal, int nSparse, int nBand, ttNum minval)
{
	int i, j, adr_jj, adr_ij, width_jj, width_ij, height, rank = 0;
	ttNum Ljj, scale;

	// sparse part, including sparse-sparse and sparse-dense
	for( j=0; j<nSparse; j++ )
	{
		// number of non-zeros to the left of (j,j)
		width_jj = MIN(j, nBand-1);

		// address of diagonal element
		adr_jj = j*nBand + nBand-1;

		// compute L(j,j)
		Ljj = mat[adr_jj] - tt_dot(mat+adr_jj-width_jj, mat+adr_jj-width_jj, width_jj);

		// compute scale = 1/tt_sqrt(MAX(L(j,j), minval)); update rank
		if( Ljj < minval )
			if ( minval )
				scale = (ttNum)(1.0/tt_sqrt(minval));
			else
				return rank;
		else
		{
			scale = (ttNum)(1.0/tt_sqrt(Ljj));
			rank++;
		}

		// number of sparse rows below j
		height = MIN(nSparse-j-1, nBand-1);

		// compute L(i,j) for i>j, sparse part
		for( i=j+1; i<=j+height; i++ )
		{
			// number of non-zeros to the left of (i,j)
			width_ij = MIN(j, nBand-1-i+j);

			// address of element (i,j)
			adr_ij = i*nBand + nBand-1-i+j;

			// in-place computation of L(i,j)
			mat[adr_ij] = scale * (mat[adr_ij] -
				tt_dot(mat+adr_jj-width_ij, mat+adr_ij-width_ij, width_ij));
		}

		// compute L(i,j) for i>j, dense part
		for( i=nSparse; i<nTotal; i++ )
		{
			// address of element (i,j)
			adr_ij = nSparse*nBand + (i-nSparse)*nTotal + j;

			// in-place computation of L(i,j)
			//  number of non-zeros to the left of (i,j) now equals width_jj
			mat[adr_ij] = scale * (mat[adr_ij] -
				tt_dot(mat+adr_jj-width_jj, mat+adr_ij-width_jj, width_jj));
		}

		// save 1/L(j,j)
		mat[adr_jj] = scale;
	}

	// dense part
	for( j=nSparse; j<nTotal; j++ )
	{
		// address of diagonal element
		adr_jj = nSparse*nBand + (j-nSparse)*nTotal + j;

		// compute Ljj
		Ljj = mat[adr_jj] - tt_dot(mat+adr_jj-j, mat+adr_jj-j, j);

		// compute scale = 1/tt_sqrt(MAX(L(j,j), minval)); update rank
		if( Ljj < minval )
			scale = (ttNum)(1.0/tt_sqrt(minval));
		else
		{
			scale = (ttNum)(1.0/tt_sqrt(Ljj));
			rank++;
		}

		// compute L(i,j) for i>j
		for( i=j+1; i<nTotal; i++ )
		{
			// address of element (i,j)
			adr_ij = adr_jj + nTotal*(i-j);

			// in-place computation of L(i,j)
			mat[adr_ij] = scale * (mat[adr_ij] - tt_dot(mat+adr_jj-j, mat+adr_ij-j, j));
		}

		// save 1/L(j,j)
		mat[adr_jj] = scale;
	}

	return rank;
}



// band-diagonal (plus dense) Cholesky backsubstitution
void tt_bandBacksub(ttNum* res, const ttNum* mat, const ttNum* vec,
					 int nTotal, int nSparse, int nBand, int nVec)
{
	int i, j, ivec, adr, adr1, width, height;
	ttNum* res_p;

	// copy if source and destination are different
	if( res!=vec )
		tt_copy(res, vec, nTotal*nVec);

	// solve for multiple vectors via backsubstitution
	for( ivec=0; ivec<nVec; ivec++ )
	{
		// precompute pointer to corresponding vector in res
		res_p = res+nTotal*ivec;

		// forward substitution: solve L*res = vec
		for( i=0; i<nTotal; i++ )
		{
			// prepare for sparse or dense
			if( i<nSparse )
			{
				width = MIN(i, nBand-1);
				adr = i*nBand + nBand-1;
			}
			else
			{
				width = i;
				adr = nSparse*nBand + (i-nSparse)*nTotal + i;
			}

			// process
			if( width )
				res_p[i] -= tt_dot(mat+adr-width, res_p+i-width, width);
			res_p[i] *= mat[adr];
		}

		// backward substitution: solve L'*res = res
		for( i=nTotal-1; i>=0; i-- )
		{
			// sparse followed by dense
			if( i<nSparse )
			{
				// number of sparse rows below (i,i), and address
				height = MIN(nSparse-i-1, nBand-1);
				adr = i*nBand + nBand-1;

				// process
				if( height )
					for( j=i+1; j<=i+height; j++ )
						res_p[i] -= mat[adr + (j-i)*(nBand-1)] * res_p[j];

				// process dense part below sparse
				adr1 = nSparse*nBand + i;
				for( j=nSparse; j<nTotal; j++ )
				{
					res_p[i] -= mat[adr1] * res_p[j];
					adr1 += nTotal;
				}
			}

			// dense only
			else
			{
				// address of (i,i)
				adr = adr1 = nSparse*nBand + (i-nSparse)*nTotal + i;

				// process
				for( j=i+1; j<nTotal; j++ )
				{
					adr1 += nTotal;
					res_p[i] -= mat[adr1] * res_p[j];
				}
			}

			// finalize
			res_p[i] *= mat[adr];
		}
	}
}



// multiply band-diagonal matrix with vector
void tt_bandMulMatVec(ttNum* res, const ttNum* mat, const ttNum* vec,
					 int nTotal, int nSparse, int nBand, int nVec)
{
	int i, ivec, adr, adr1, width;
	ttNum* res_p;
	const ttNum* vec_p;

	// handle multiple vectors
	for( ivec=0; ivec<nVec; ivec++ )
	{
		// precompute pointer to corresponding vector in res
		res_p = res+nTotal*ivec;
		vec_p = vec+nTotal*ivec;

		for( i=0; i<nSparse; i++ )
		{
			width = MIN(i+1, nBand);
			adr = i*nBand + nBand - width;
			adr1 = MAX(0, i-nBand+1);
			res_p[i] = tt_dot(mat+adr, vec_p+adr1, width);
			tt_addToScl(res_p+adr1, mat+adr, vec_p[i], width-1);

		}

		for( i=nSparse; i<nTotal; i++ )
		{
			adr = nSparse*nBand + i*nTotal;
			res_p[i] = tt_dot(mat+adr, res_p, nTotal);
		}
	}
}




//------------------------------ actuator models ------------------------------


// smooth abs, output y = sqrt(x^2+beta^2)-beta, yx= dy/dx
ttNum tt_softAbs(ttNum x, ttNum beta, ttNum *yx)
{
	ttNum tmp;
	if( beta==0 ) // hard abs
	{
		if ( yx )
			*yx = (x >= 0 ? 1 : -1);
		return (x >= 0 ? x : -x);
	}
	else          // smooth abs
	{
		tmp   = sqrt(x*x + beta*beta);
		if ( yx )
			*yx   = x / tmp;
		return tmp - beta;
	}
}

// long-tailed sigmoid, y = x/sqrt(x^2+beta^2)
ttNum tt_sigmoid(ttNum x, ttNum beta, ttNum *yx,  ttNum *yxx)
{
	ttNum s, y;
	if( beta==0 )       // step function
	{
		if (yx) *yx = 0;
		return (x > 0 ? 1 : -1);
	}
	else                // sigmoid
	{
		s	= 1/sqrt(x*x + beta*beta);
		y	= s*x;
		if (yx)
			*yx  = s * (1-y*y);
		if (yxx)
			*yxx  = -3 * s * y * yx[0];
		return y;
	}
}

// hard-clamp vector to range [-limit(i), +limit(i)]
void tt_clampVec(ttNum* vec, const ttNum* limit, int n)
{
	int i;

	// loop over active limits
	for( i=0; i<n; i++ )
		if( limit[i]>0 )
		{
			if( vec[i] < -limit[i] )
				vec[i] = -limit[i];
			else if( vec[i] > limit[i] )
				vec[i] = limit[i];
		}
}


// print matrix to screen
void tt_matPrint(const ttNum* mat, int nr, int nc)
{
	int r, c;
	for( r=0; r<nr; r++ )
	{
		for( c=0; c<nc; c++ )
			printf("%.8f ", mat[r*nc+c]);
		printf("\n");
	}
	printf("\n");
}



// min function, avoid re-evaluation
ttNum tt_min(ttNum a, ttNum b)
{
	if( a <= b )
		return a;
	else
		return b;
}



// max function, avoid re-evaluation
ttNum tt_max(ttNum a, ttNum b)
{
	if( a >= b )
		return a;
	else
		return b;
}


// sign function
ttNum tt_sign(ttNum x)
{
	if( x<0 )
		return -1;
	else if( x>0 )
		return 1;
	else
		return 0;
}


// round to nearest integer
int tt_round(ttNum x)
{
	ttNum lower = tt_floor(x);
	ttNum upper = tt_ceil(x);

	if( x-lower < upper-x )
		return (int)lower;
	else
		return (int)upper;
}

#ifdef _NIX
#define _finite finite
#endif

// return 1 if nan or abs(x)>ttMAXVAL, 0 otherwise
int tt_isBad1(ttNum x)
{
	return( !_finite(x) || x>ttMAXVAL || x<-ttMAXVAL );
}

ttByte tt_isBad(const ttNum* x, int n)
{
	int i;
	for( i=0; i<n ; i++ )
		if( tt_isBad1(x[i]) )
			return 1;
	return 0;
}

// Write to a file
void tt_fWrite(FILE* fp, ttNum* data, int n, int newline_at_end)
{
	int i;
	for(i = 0; i<n; i++)
		fprintf(fp, "%f,\t", data[i]);

	if (newline_at_end)
		fprintf(fp, "\n");
}

// return 1 if all elements are 0
int tt_isZero(ttNum* vec, int n)
{
	int i;

	for( i=0; i<n; i++ )
		if( vec[i]!=0 )
			return 0;

	return 1;
}



// rescaling:  y = MAX(minval, s0 + s1*x)
ttNum tt_rescale(const ttNum* s, ttNum x, ttNum minval)
{
	return tt_max(minval, s[0] + s[1] * x);
}


// smooth max function
ttNum tt_softMax(ttNum x, ttNum y, ttNum beta)
{
	return (x + y + tt_softAbs(x-y, beta, 0))/2;
}

// smooth min function
ttNum tt_softMin(ttNum x, ttNum y, ttNum beta)
{
	return (x + y - tt_softAbs(x-y, beta, 0))/2;
}


ttNum tt_rand_gauss(void)
{
	ttNum v1, v2, s;

	do
	{
		v1 = 2.0 * ((ttNum) rand()/RAND_MAX) - 1;
		v2 = 2.0 * ((ttNum) rand()/RAND_MAX) - 1;
		s  = v1*v1 + v2*v2;
	} while ( s >= 1.0 );

	if (s == 0.0)
		return 0.0;
	else
		return (v1*tt_sqrt(-2.0 * tt_log(s) / s));
}


void tt_thread_schedule(int schedule[][2], int nOperation, int nThread)
{
	int i,blksz, extra;
	// prepare thread schedule
	blksz = nOperation/nThread;
	extra = nOperation - blksz*nThread;
	for( i=0; i<nThread; i++ )
	{
		schedule[i][0] = (i ? schedule[i-1][1] : 0);
		schedule[i][1] = schedule[i][0] + blksz + (i<extra);
	}
}
