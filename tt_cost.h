#pragma once

//#include "mj_engine.h"
#include "tt_norm.h"
#include "tt_trajectory.h"


#define MAX_N_TERM	32
#define MAX_N_TRANSITIONS	3
#define MAX_N_ALTERS	50

#pragma pack(push, 1)

// ========= callback types =========
typedef int (*rollout_fun) (void* cbData, const trajectory *policy, trajectory **candidates);
typedef int (*derivative_fun) (void* cbData, trajectory *T);

typedef enum _ttVerbosity
{
	verboseSILENT  = 0,
	verboseWARNING,
	verboseFINAL,
	verboseITER,
	verboseDEBUG
} ttVerbosity;

typedef struct _mjOptOpt
{
	// general
	ttVerbosity	verbosity;

	// parallelization
	int		nThreads;		// number of threads

	// trajectory options
	int		N;				// horizon length
	ttNum  discount;		// discounting
	ttNum  dt_start;		// size of initial timestep
	int		n_dt_start;		// number of timesteps to use dt_start
	int		n_dt_interp;	// number of timesteps to interpolate from dt_start to dt

	// findiff options
	int 	log2eps;		// log2(epsilon)
	int 	central;		// 0: forward; 1: central
	int		extrap_lc_dist;	// skip collision detection, extrapolate distances
	int		iter_flc_f;		// number of iterations for flc_f in cloud points
	int		act_jac;		// use analytic Jacobian for act variables

	// Levenberg-Marquardt options
	int		maxIter;		// maximum number of iterations
	ttNum	muInit;			// initial value for mu   yuval: why?
	ttNum 	muMin;			// minimum value for mu
	ttNum 	muMax;			// above this value is considered failure
	ttNum 	muFactor;		// coefficient for changing dmu
	ttNum	zBad;			// below this value increase mu
	ttNum	zGood;			// above this value decrease mu
	ttNum	alphaBad;		// above this value decrease mu
	ttNum	alphaGood;		// above this value decrease mu
	ttNum	minAlpha;		// smallest line-search alpha
	ttNum	tolX;			// exit tolerance: change of x
	ttNum	tolG;			// exit tolerance: norm of gradient
	ttNum	tolFun;			// exit tolerance: change of objective function
	ttByte	sparseHess;		// Hessian with arrow-shaped sparsity pattern

	// iLQG options
	int		maxBpass;		// maximum number of backpasses
	int		nRollout;		// number of rollouts
	int 	regType;		// regularization type

	// estimation options
	ttNum	procNoise;		// maximum number of backpasses
	ttNum	obsNoise;		// number of rollouts

	// callbacks
	rollout_fun cb_rollout;
	derivative_fun cb_derivatives;
} mjOptOpt;



typedef struct _mjtTerm
{
	//char	name[STR_LENGTH];
	int			nr;
	int			f_inx;
	int			r_adr;
	mjtNormType	norm;
	ttByte		isloss;
	ttNum		params[MAX_NORM_PARAM];
	ttNum		coef_running;
	ttNum		coef_final;
} mjtTerm;


typedef struct _mjtTransition
{
	int			nr;
	int			f_inx;
	int			f_adr;
	mjtNormType	norm;
	ttByte		isloss;
	ttNum		params[MAX_NORM_PARAM];
	ttNum		threshold;
	int			to_cost;
} mjtTransition;

typedef struct _mjtAlter
{
	int		nr;
	int		f_inx;
	int		f_adr;
	int		buffer_offset;
} mjtAlter;


typedef struct _mjCost
{
	int				nr;
	int				n_auxf;
	int				plot_f_inx;
	int				plot_nf;
	int				nterms;
	int				ntransitions;
	int				nalters;
	ttNum			entry_time;
	mjtTerm			term[MAX_N_TERM];
	mjtTransition	transition[MAX_N_TRANSITIONS];
	mjtAlter		alter[MAX_N_ALTERS];
	mjtTransition	dataTransition;
	mjOptOpt		optopt;
	//void *			opt;
} mjCost;


void muControl(mjoTrace *t, const mjOptOpt o);
void muScale(mjoTrace *t, const mjOptOpt o, ttNum factor);

#ifdef __cplusplus
extern "C"
{
#endif

extern mjtNorm funList[NUM_NORM];

void defaultOptOpt(mjOptOpt* optopt);
ttNum cost_step(trajElement *e, const mjCost* cost, int nx, int ndx, int nu, ttByte isFinal, ttNum t0, ttNum* scratch, int szScratch);

#ifdef __cplusplus
}
#endif

#pragma pack(pop)
