#pragma once

// extensions
#include "tt_trajectory.h"
#include "tt_boxQP.h"
#include "tt_util.h"


// third party
#ifndef __APPLE__
#include "omp.h"
#endif

// ========= constants =========
#define MAX_THREADS 24	// maximum number of threads fo MPC machinery

// ========= functions =========
#ifdef __cplusplus
extern "C"
{
#endif

void set_dt(trajectory *T, const mjOptOpt optopt, ttNum dt);
mjoResult iLQG(void* cbData, trajectory* policy, trajectory **candidates, mjOptOpt* options,
			   ttNum* scratch, int scratchSz);

mjoResult iLQGsolve(void* cbData, trajectory* policy, trajectory **candidates, mjOptOpt* options,
				trajectory* Tscratch);

int back_pass(trajectory *T, int regType, ttNum* scratch, int szScratch);

int riccati(int n, int m, ttNum mu, int regType,
			const ttNum *Wx, const ttNum *Wxx,
			const ttNum *yx, const ttNum *yu,
			const ttNum *cx, const ttNum *cu,
			const ttNum *cxx, const ttNum *cxu, const ttNum *cuu,
			const ttNum* lower, const ttNum* upper, const ttNum *next_l,
			ttNum *Vx, ttNum *Vxx, ttNum *du, ttNum *L,
			ttNum *dV,
			ttNum *Qx, ttNum *Qu, ttNum *Qxx, ttNum *Qxu, ttNum *Quu,
			ttNum *scratch);

void makeRollouts(trajectory* policy, trajectory **candidates, mjOptOpt* option, ttNum t0, const ttNum* x0);

//int initialRollouts(void* cbData, trajectory* policy, trajectory **candidates, mjOptOpt* options, ttNum t0, const ttNum* x0);
void prepareCandidates(trajectory* policy, trajectory **candidates, mjOptOpt* option, ttNum t0, const ttNum* x0);

int multiRollout(void* cbData, const trajectory *policy, trajectory **candidates);

int trajDerivatives(void* cbData, trajectory *T);

#ifdef __cplusplus
}
#endif
