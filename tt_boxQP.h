#pragma once

#include "tt_util.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include "math.h"

int boxQP(int *dim, ttNum* x, ttNum** _R, ttNum** _index,// outputs
		ttNum* scratch,	const int szScratch,			// scratch space
		const ttNum* H,	const ttNum* g,				// quadratic program
		const ttNum* lower,const ttNum* upper,			// bounds
		const ttNum* x0,	const ttNum* options);			// optional inputs



#ifdef __cplusplus
}
#endif


