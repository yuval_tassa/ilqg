#pragma once
#include "stdio.h"
#include "math.h"

#define ttMKL_MUL
//#define ttMKL_CHOL
//#define ttVERBOSE

#define ttPI			3.14159265358979323846
#define ttMINVAL_F		1E-10		// minimum value allowed in any denominator, float
#define ttMINVAL_D		1E-14		// minimum value allowed in any denominator, double
#define ttMAXVAL		1E+15		// maximum value allowed in qpos, qvel (for autofix)
#define ttMINMU		1E-5		// minimum friction coefficient
#define ttMINREG		1E-10		// minimum diagonal regularier for eq_A and flc_A


#define ttUSEDOUBLE

#ifdef ttUSEDOUBLE
	typedef double ttNum;			// numeric data type (float or double)

	static const double ttMINVAL = ttMINVAL_D;
	#define tt_sqrt sqrt
	#define tt_exp exp
	#define tt_sin sin
	#define tt_tan tan
	#define tt_cos cos
	#define tt_acos acos
	#define tt_asin asin
	#define tt_atan2 atan2
	#define tt_tanh tanh
	#define tt_pow pow
	#define tt_abs fabs
	#define tt_log log
	#define tt_floor floor
	#define tt_ceil ceil

#else
	typedef float ttNum;

	static const float ttMINVAL = (float)ttMINVAL_F;
	#define tt_sqrt sqrtf
	#define tt_exp expf
	#define tt_sin sinf
	#define tt_cos cosf
	#define tt_tan tanf
	#define tt_acos acosf
	#define tt_asin asinf
	#define tt_atan2 atan2f
	#define tt_tanh tanhf
	#define tt_pow powf
	#define tt_abs fabsf
	#define tt_log log
	#define tt_floor floorf
	#define tt_ceil ceilf
#endif

// max and min macros
#define MAX(a,b)    (((a) > (b)) ? (a) : (b))
#define MIN(a,b)    (((a) < (b)) ? (a) : (b))

// inf and nan
#define tt_inf -tt_log(0.0)
#define tt_nan tt_sqrt(-1.0)

#ifdef __cplusplus
extern "C"
{
#endif

typedef unsigned char ttByte;		// used for true/false

//------------------------------ user handlers --------------------------------------

extern void (*tt_user_error)(const char*);
extern void (*tt_user_warning)(const char*);
extern void* (*tt_user_malloc)(unsigned int, unsigned int);
extern void (*tt_user_free)(void*);

// restore default handlers
void tt_clear_handlers(void);

ttNum* tt_stackAlloc(ttNum **scratch, int *szScratch, int size);

//------------------------------ errors and warnings --------------------------------

// write text message to logfile and console, exit if error
void tt_error(const char* msg);
void tt_warning(const char* msg);

// additional int argument
void tt_error_i(const char* msg, int i);
void tt_warning_i(const char* msg, int i);

// additional string argument
void tt_error_s(const char* msg, const char* text);
void tt_warning_s(const char* msg, const char* text);

// write datetime, type: message to MUJOCO_LOG.TXT
void tt_write_log(const char* type, const char* msg);


//------------------------------ malloc and free ------------------------------------

// allocate memory, align if > 0
void* tt_malloc(unsigned int sz, unsigned int align);

// free memory, using free() by default
void tt_free(void* ptr);



void tt_copyDF(ttNum *dest, const float *source, int n);


void tt_copyFD(float *dest, const ttNum *source, int n);


//------------------------------ 3D vector and matrix-vector operations --------------

// set vector to zero
void tt_zero3(ttNum* res);

// copy vector
void tt_copy3(ttNum* res, const ttNum* data);

// scale vector
void tt_scl3(ttNum* res, const ttNum* vec, ttNum scl);

// add vectors
void tt_add3(ttNum* res, const ttNum* vec1, const ttNum* vec2);

// subtract vectors
void tt_sub3(ttNum* res, const ttNum* vec1, const ttNum* vec2);

// add to vector
void tt_addTo3(ttNum* res, const ttNum* vec);

// add scaled to vector
void tt_addToScl3(ttNum* res, const ttNum* vec, ttNum scl);

// normalize vector, return length before normalization
ttNum tt_normalize3(ttNum* res);

// compute vector length (without normalizing)
ttNum tt_norm3(const ttNum* res);

// vector dot-product
ttNum tt_dot3(const ttNum* vec1, const ttNum* vec2);

// Cartesian distance between 3D vectors
ttNum tt_dist3(const ttNum* pos1, const ttNum* pos2);

// multiply vector by 3D rotation matrix
void tt_rotVecMat(ttNum* res, const ttNum* vec, const ttNum* mat);

// multiply vector by transposed 3D rotation matrix
void tt_rotVecMatT(ttNum* res, const ttNum* vec, const ttNum* mat);


//------------------------------ general vector operations ---------------------------

// set vector to zero
void tt_zero(ttNum* res, int n);

// set matrix to diagonal
void tt_diag(ttNum* mat, ttNum value, int n);

// copy vector
void tt_copy(ttNum* res, const ttNum* data, int n);

// move vector
void tt_move(ttNum* res, const ttNum* vec, int n);

// scale vector
void tt_scl(ttNum* res, const ttNum* vec, ttNum scl, int n);

//fill vector with a value
void tt_fill(ttNum* vec, const ttNum val, const int n);

// sum vector
ttNum tt_sum(const ttNum* vec, int n);

// add vectors
void tt_add(ttNum* res, const ttNum* vec1, const ttNum* vec2, int n);

// subtract vectors
void tt_sub(ttNum* res, const ttNum* vec1, const ttNum* vec2, int n);

// add to vector
void tt_addTo(ttNum* res, const ttNum* vec, int n);

// add scaled to vector
void tt_addToScl(ttNum* res, const ttNum* vec, ttNum scl, int n);

// normalize vector, return length before normalization
ttNum tt_normalize(ttNum* res, int n);

// compute vector length (without normalizing)
ttNum tt_norm(const ttNum* res, int n);

// vector dot-product
ttNum tt_dot(const ttNum* vec1, const ttNum* vec2, const int n);


//------------------------------ matrix-vector operations ----------------------------

// multiply matrix and vector
void tt_mulMatVec(ttNum* res, const ttNum* mat, const ttNum* vec,
				   int nr, int nc);

// multiply transposed matrix and vector
void tt_mulMatTVec(ttNum* res, const ttNum* mat, const ttNum* vec,
					int nr, int nc);

// normalize columns of matrix
void tt_normalizeCol(ttNum* mat, int nr, int nc);


//------------------------------ matrix-matrix operations ----------------------------

// identity matrix
void tt_eye(ttNum* mat, int n);

// transpose matrix
void tt_transpose(ttNum* res, const ttNum* mat, int r, int c);

// add transpose matrix to scaled matrix
void tt_transposeAdd(ttNum* res, const ttNum* mat, int r, int c);

// enforce symmetry
void tt_symmetrize(ttNum* mat, int n);

// multiply matrices
void tt_mulMatMat(ttNum* res, const ttNum* mat1, const ttNum* mat2,
				   int r1, int c1, int c2);

// multiply matrices, second argument transposed
void tt_mulMatMatT(ttNum* res, const ttNum* mat1, const ttNum* mat2,
				    int r1, int c1, int r2);

// compute M'*M
void tt_sqrMatT(ttNum* res, const ttNum* mat, int r, int c);

// compute M*M'
void tt_sqrMat(ttNum* res, const ttNum* mat, int r, int c,
				ttNum* scratch, int szScratch);

// compute M1*M2 + M2'*M1'
void tt_sqrMatMat(ttNum* res, const ttNum* mat1, const ttNum* mat2, int r, int c,
				   ttNum* scratch, int szScratch);

// multiply matrices, first argument transposed
void tt_mulMatTMat(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					int r1, int c1, int c2);

// multiply matrices, sparse
void tt_mulMatMat_S(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					 int r1, int c1, int c2, ttNum* scratch, int szScratch);

// multiply matrices, second argument transposed, sparse
void tt_mulMatMatT_S(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					  int r1, int c1, int r2, ttNum* scratch, int szScratch);

// sparse symmetric matrix multiplication, second argument transposed
void tt_mulMatMatT_SS(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					   int r, int c, ttNum* scratch, int szScratch);

// sparse symmetric matrix multiplication, first argument transposed
void tt_mulMatTMat_SS(ttNum* res, const ttNum* mat1, const ttNum* mat2,
					   int r, int c, ttNum* scratch, int szScratch);

// rank-one update to symmetric matrix, enforce symmetry
void tt_rankOneUpdateSym(ttNum* res, const ttNum* vec1, const ttNum* vec2,
						  ttNum scl, int n);


//------------------------------ tensor operations ----------------------------

// multiply vector by tensor, output matrix
void tt_multVecTens(ttNum* res, const ttNum* vec, const ttNum* tens,
				   const int r, const int c, const int p);

// multiply vector by symmetric tensor, output symmetric matrix
void tt_multVecSymTens(ttNum* res, const ttNum* vec, const ttNum* tens,
				   const int r, const int c);


//------------------------------ quaternion operations -----------------------------

// rotate vector by quaternion
void tt_rotVecQuat(ttNum* res, const ttNum* vec, const ttNum* quat);

// muiltiply quaternions
void tt_mulQuat(ttNum* res, const ttNum* quat1, const ttNum* quat2);

// negate quaternion
void tt_negQuat(ttNum* res, const ttNum* quat);

// convert axisAngle to quaternion
void tt_axisAngle2Quat(ttNum* res, const ttNum* axis, ttNum angle);

// convert quaternion (corresponding to orientation difference) to 3D velocity
void tt_quat2Vel(ttNum* res, const ttNum* quat, ttNum dt);

// convert quaternion to 3D rotation matrix
void tt_quat2Mat(ttNum* res, const ttNum* quat);

// convert 3D rotation matrix to quaterion
void tt_mat2Quat(ttNum* quat, const ttNum* mat);

// time-derivative of quaternion, given 3D rotational velocity
void tt_derivQuat(ttNum* res, const ttNum* quat, const ttNum* vel);

// integrate quaterion given 3D angular velocity
void tt_quatIntegrate(ttNum* quat, const ttNum* vel, ttNum scale);

// compute quaternion performing rotation from given vector to z-axis
void tt_quatZ2Vec(ttNum* quat, const ttNum* vec);


//------------------------------ spatial algebra --------------------------------

// vector cross-product, 3D
void tt_cross(ttNum* res, const ttNum* a, const ttNum* b);

// cross-product for motion vector
void tt_crossMotion(ttNum* res, const ttNum* vel, const ttNum* v);

// cross-product for force vectors
void tt_crossForce(ttNum* res, const ttNum* vel, const ttNum* f);

// express inertia in com-based frame
void tt_inertCom(ttNum* res, const ttNum* inert, const ttNum* mat,
				  const ttNum* dif, ttNum mass);

// express motion axis in com-based frame
void tt_dofCom(ttNum* res, const ttNum* axis, const ttNum* offset);

// multiply 6D vector (rotation, translation) by 6D inertia matrix
void tt_mulInertVec(ttNum* res, const ttNum* inert, const ttNum* vec);

// multiply dof matrix by vector
void tt_mulDofVec(ttNum* res, const ttNum* mat, const ttNum* vec, int n);

// transform 6D motion or force vector between frames
//  rot is 3-by-3 matrix; flg_force determines vector type (motion or force)
void tt_transformSpatial(ttNum* res, const ttNum* vec, ttByte flg_force,
						  const ttNum* newpos, const ttNum* oldpos,
						  const ttNum* rotnew2old);


//------------------------------ tendon wrapping ------------------------------

// wrap tendons around spheres and cylinders
ttNum tt_wrap(ttNum* wpnt, const ttNum* x0, const ttNum* x1,
				const ttNum* xpos, const ttNum* xmat, const ttNum* size,
				int type, const ttNum* side);


//------------------------------ linear solvers -------------------------------

// invert positive-definite matrix
int tt_inverse(ttNum* inv, ttNum* temp, const ttNum* mat, int n);

// Cholesky decomposition
int tt_cholFactor(ttNum* mat, ttNum* diag, int n,
				   ttNum minabs, ttNum minrel, ttNum* correct);

// Cholesky backsubstitution (or half of it)
void tt_cholBacksub(ttNum* res, const ttNum* mat, const ttNum* vec,
					 int n, int nvec, ttByte half);



// band-diagonal (plus dense) Cholesky decomposition
int tt_bandFactor(ttNum* mat, int nTotal, int nSparse, int nBand, ttNum minval);



// band-diagonal (plus dense) Cholesky backsubstitution
void tt_bandBacksub(ttNum* res, const ttNum* mat, const ttNum* vec,
					 int nTotal, int nSparse, int nBand, int nVec);



// multiply band-diagonal matrix with vector
void tt_bandMulMatVec(ttNum* res, const ttNum* mat, const ttNum* vec,
					 int nTotal, int nSparse, int nBand, int nVec);


//------------------------------ matrix decompositions ------------------------

// eigenvalue decomposition of symmetric 3x3 matrix
int tt_eig3(ttNum* eigval, ttNum* eigvec, ttNum* quat, const ttNum* mat);

// singular value decomposition of 3x3 matrix
void tt_svd3(ttNum* U, ttNum* s, ttNum* V, const ttNum* mat);


//------------------------------ miscellaneous --------------------------------

// make 3D frame given X axis (and possibly Y axis)
void tt_makeFrame(ttNum* frame);

// enforce friction cone
void tt_clampCone(ttNum* f, const ttNum* friction, int dim);

// enforce friction pyramid
void tt_clampPyramid(ttNum* f, const ttNum* friction, int dim);

// hard-clamp vector to range [-limit(i), +limit(i)]
void tt_clampVec(ttNum* vec, const ttNum* limit, int n);

// compute desired/minimal next-state velocity of critically-damped spring
ttNum tt_errReduce(ttNum pos, ttNum vel, ttNum tau, ttNum dt, ttByte onesided);

// set x = nearest point to y within Cone(mu, rn, rf)
// return 0: x = y (y feasible), 1: x on cone surface; 2: x = 0 (tip of cone)
int tt_projectCone(ttNum* x, const ttNum* y,
					const ttNum* mu, ttNum rn, ttNum rf, int dim);

// muscle FVL curve:  prm = (lminrel, lmaxrel, widthrel, vmaxrel, fmax, fvsat)
ttNum tt_muscleFVL(ttNum len, ttNum vel, ttNum lmin, ttNum lmax, ttNum* prm);

// muscle passive force:  prm = (lminrel, lmaxrel, fpassive)
ttNum tt_musclePassive(ttNum len, ttNum lmin, ttNum lmax, ttNum* prm);

// pneumatic cylinder dynamics
ttNum tt_pneumatic(ttNum pressure, ttNum ctrl, ttNum len, ttNum vel, ttNum* prm, ttNum dt, ttNum* dpdu);
// emo's interface:
//ttNum tt_pneumatic(ttNum len, ttNum len0, ttNum vel, ttNum* prm,
//					 ttNum act, ttNum ctrl, ttNum timestep, ttNum* jac);

// solve linear ODE
ttNum tt_linear_ODE(ttNum p, ttNum a, ttNum r, ttNum t, int n, ttNum *derivs);

ttNum tt_sigmoid(ttNum x, ttNum beta, ttNum *yx,  ttNum *yxx);


// print matrix to screen
void tt_matPrint(const ttNum* mat, int nr, int nc);

// min function, single evaluation of a and b
ttNum tt_min(ttNum a, ttNum b);

// max function, single evaluation of a and b
ttNum tt_max(ttNum a, ttNum b);

// sign function
ttNum tt_sign(ttNum x);

// round to nearest integer
int tt_round(ttNum x);

// convert type id to type name
const char* tt_type2Str(int type);

// return 1 if nan or abs(x)>mjMAXVAL, 0 otherwise
int tt_isBad1(ttNum x);

// a vectorized isbad function
ttByte tt_isBad(const ttNum* x, int n);

// return 1 if all elements are 0
int tt_isZero(ttNum* vec, int n);

// rescaling:  y = MAX(minval, s0 + s1*x)
ttNum tt_rescale(const ttNum* s, ttNum x, ttNum minval);

// smooth max function
ttNum tt_softMax(ttNum x, ttNum y, ttNum beta);

// smooth min function
ttNum tt_softMin(ttNum x, ttNum y, ttNum beta);

// smooth abs, output y = sqrt(x^2+beta^2)-beta, yx= dy/dx
ttNum tt_softAbs(ttNum x, ttNum beta, ttNum *yx);

// gaussian random number generator
ttNum tt_rand_gauss(void);

// prepare thread schedule
void tt_thread_schedule(int schedule[][2], int nOperation, int nThread);

#ifdef __cplusplus
}
#endif
