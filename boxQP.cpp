#include "tt_boxQP.h"

#ifdef _MEX
	#include "mex.h"
	#define tt_printf mexPrintf
#else
   #include <stdio.h>
	#define tt_printf printf
#endif

/*------- boxQP:  minimize 0.5*x'*H*x + x'*g  s.t. lower <= x <=upper ----------
 inputs:
	n		 	- problem dimension
	H			- positive definite matrix	(n * n)
	g			- bias vector				(n)
	lower		- lower bounds				(n)
	upper		- upper bounds				(n)
	scratch		- scratch space
	szScratch	- size of scratch space		(>= n^2 + 7*n)
  optional inputs:
	x0			- initial state				(n)
	options		- see below					(7)
 outputs:
	result		- result type (roughly higher is better, see below)
	n_free <= n	- size of solution subspace
	x			- solution					(n)
 optional outputs:
	R			- subspace cholesky factor	(n_free * n_free)
	index		- set of free dimensions	(n_free)
 notes:
	the 1st argument *_n encodes both input (n) and output (n_free) dimensions.
	only x and scratch need pre-allocation,	the optional outputs R and index
	will point into the scratch space.
	index is notionally an int* but cast as ttNum*.
----------------------------------- boxQP ------------------------------------*/
int boxQP(int *_n, ttNum* x, ttNum** _R, ttNum** _index,	// outputs
		ttNum* scratch,	const int szScratch,			// scratch space
		const ttNum* H,	const ttNum* g,				// quadratic program
		const ttNum* lower,const ttNum* upper,			// bounds
		const ttNum* x0,	const ttNum* options)			// optional inputs
{
	int i, j, nstep, rank, nFree, result=0, iter=0, nFactor=0;
	int factorize=1; // always factorize in the first iteration
	ttNum norm, oldvalue, step, sdotg, improvement=0.0, value=0.0;
	ttNum	*grad, *search, *candidate, *temp, *clamped, *oldclamped, *ptr;
	ttNum	*R, *index;
	int n = *_n;	// get the dimension of the QP

	const char result_type[8][50]= {
		"Hessian is not positive definite",          // result = -1
		"No descent direction found",                // result = 0    SHOULD NOT OCCUR
		"Maximum main iterations exceeded",          // result = 1
		"Maximum line-search iterations exceeded",   // result = 2
		"No bounds, returning Newton point",         // result = 3
		"Improvement smaller than tolerance",        // result = 4
		"Gradient norm smaller than tolerance",      // result = 5
		"All dimensions are clamped"};               // result = 6

	// algorithm defaults
	int		maxIter			= 100;		// maximum number of iterations
	ttNum	minGrad			= 1E-8;		// minimum norm of non-clamped gradient
	ttNum	minRelImprove	= 1E-8;		// minimum relative improvement
	ttNum	stepDec			= 0.6;		// factor for decreasing stepsize
	ttNum	minStep			= 1E-22;	// minimal stepsize for linesearch
	ttNum	Armijo			= 0.1;		// Armijo parameter (fraction of linear improvement)
	int     verbosity		= 0;		// 0:none; 1:final; 2:iter; 3:solution

	// set options
	if (options)
	{
		maxIter				= (int) options[0];
		minGrad				= options[1];
		minRelImprove		= options[2];
		stepDec				= options[3];
		minStep				= options[4];
		Armijo				= options[5];
		verbosity			= (int) options[6];
	}

	if( n<=0 )
		tt_error("boxQP - positive n expected");

	// assign optional outputs and scratch vectors to scratch space
	if( szScratch < n*n + 7*n)
		tt_error("boxQP - insufficient scratch size");
	ptr			= scratch;
	R			= ptr; ptr += n*n;
	index		= ptr; ptr += n;
	grad		= ptr; ptr += n;
	search		= ptr; ptr += n;
	candidate	= ptr; ptr += n;
	temp		= ptr; ptr += n;
	clamped		= ptr; ptr += n;
	oldclamped	= ptr; ptr += n;

	if( !lower && !upper )		// no bounds: return Newton point
	{
		nFree = n;
		// try to factorize
		tt_copy(R, H, n*n);
		rank = tt_cholFactor(R, x, n,0,0,0);
		if (rank == n)
		{
			tt_cholBacksub(x, R, g, n, 1, 0);
			tt_scl(x, x, -1, n);
			nFactor	= 1;
			result	= 3;
		}
		else
			result	= -1;
		// full index set (no clamping)
		for ( i=0; i<n; i++ )
			index[i] = (ttNum)i;
	}
	else						// bounds: initialize x
	{
		if( x0 )
		{
			tt_copy(x, x0, n);						// x = clamp(x0)
			for( i=0; i<n; i++ )
			{
				x[i] = tt_min(x[i], upper[i]);
				x[i] = tt_max(x[i], lower[i]);
			}
		}
		else if( lower && upper )
			for( i=0; i<n; i++ )
				x[i] = (lower[i] + upper[i])/2;     // x = (lower + upper)/2
		else if( lower )
			for( i=0; i<n; i++ )
				x[i] = lower[i] + 1;                // x = lower + 1
		else
			for( i=0; i<n; i++ )
				x[i] = upper[i] - 1;                // x = upper - 1
	}

	/*------------------------------- main loop ------------------------------*/
	for( iter=1; iter<maxIter; iter++ )
	{
		if ( result )
			break;

		// compute objective value = .5*x'*H*x + x'*g
		tt_mulMatVec(temp, H, x, n, n);
		value = 0.5 * tt_dot(x, temp, n) + tt_dot(x, g, n);

		// improvement too small: minimum found
		if( iter>1 && oldvalue - value < minRelImprove*fabs(oldvalue) )
		{
			result = 4;
			break;
		}

		// save last value
		oldvalue = value;

		// compute gradient
		tt_mulMatVec(grad, H, x, n, n);
		tt_addTo(grad, g, n);

		/*----------------------- find clamped dimensions --------------------*/
		for( i=0; i<n; i++ )
			if( ( lower && x[i] == lower[i] && grad[i] > 0 )||
				( upper && x[i] == upper[i] && grad[i] < 0 ) )
				clamped[i] = 1;
			else
				clamped[i] = 0;

		// build index of free dimensions, count them
		nFree	= 0;
		for( i=0; i<n; i++ )
			if( !clamped[i] )
				index[nFree++] = (ttNum)i;

		// all dimensions are clamped: minimum found
		if( !nFree )
		{
			result = 6;
			break;
		}

		// factorize if clamped has changed
		if( iter > 1 )
		{
			factorize = 0;
			for( i=0; i<n; i++ )
			{
				if( clamped[i] != oldclamped[i] )
				{
					factorize = 1;
					break;
				}
			}
		}

		// save last clamped
		for( i=0; i<n; i++ )
			oldclamped[i] = clamped[i];

		/*------------------------ get search direction ----------------------*/
		// search = g + H_all,clamped * x_clamped
		for( i=0; i<n; i++ )
			temp[i] = clamped[i] ? x[i] : 0 ;
		tt_mulMatVec(search, H, temp, n, n);
		tt_addTo(search, g, n);

		// search = compress_free[search]
		for( i=0; i<nFree; i++ )
			search[i] = search[(int)index[i]];

		// R = compress_free[H]
		if (factorize)
			for( i=0; i<nFree; i++ )
				for( j=0; j<nFree; j++ )
					R[i*nFree+j] = H[(int)index[i]*n+(int)index[j]];

		// factorize and increment counter
		rank	 = factorize ? tt_cholFactor(R, temp, nFree,0,0,0) : nFree;
		nFactor += factorize;

		// abort if factorization failed
		if (rank != nFree){
			result = -1;
			break;
		}

		// temp = H_free,free \ search_free
		tt_cholBacksub(temp, R, search, nFree, 1, 0);

		// search_free = expand(-temp) - x_free
		tt_zero(search, n);
		for( i=0; i<nFree; i++ )
			search[(int)index[i]] = -temp[i] -x[(int)index[i]];

		/*-------------------------- check gradient --------------------------*/
		// norm of free gradient
		norm = 0;
		for( i=0; i<n; i++ )
			if( !clamped[i] )
				norm += grad[i]*grad[i];
		norm = tt_sqrt(norm);

		// gradient too small: minimum found
		if( norm<minGrad )
		{
			result = 5;
			break;
		}

		// sanity check: make sure we have a descent direction
		if( (sdotg = tt_dot(search, grad, n)) >= 0 )
			break; // SHOULD NOT OCCUR

		/*--------------------- projected Armijo line search -----------------*/
		step  = 1;
		nstep = 0;
		do{
			// candidate = clamp(x + step*search)
			tt_scl(candidate, search, step, n);
			tt_addTo(candidate, x, n);
			for( i=0; i<n; i++ )
			{
				if( lower && candidate[i]<lower[i] )
					candidate[i] = lower[i];
				else if( upper && candidate[i]>upper[i] )
					candidate[i] = upper[i];
			}

			// new objective value
			tt_mulMatVec(temp, H, candidate, n, n);
			value = 0.5 * tt_dot(candidate, temp, n) + tt_dot(candidate, g, n);

			// increment and break if step is too small
			nstep++;
			step = step*stepDec;
			if( step<minStep )
			{
				result = 2;
				break;
			}

			// repeat until relative improvement >= Armijo
			improvement = (value - oldvalue) / (step*sdotg);
		} while( improvement < Armijo );


		// print iteration info
		if( verbosity > 1 )
			tt_printf("iter %3d:  |grad|= %9.3g,  reduction= %9.3g, improvement= %9.4g,  linesearch= %g ^ %-2d,  factorize= %d\n",
					iter, norm, oldvalue-value, improvement, stepDec, nstep, factorize);

		// accept candidate
		tt_copy(x, candidate, n);
	}

	// max iterations exceeded
	if( iter>=maxIter )
		result = 1;

	// print final info
	if( verbosity > 0 )
	{
		tt_printf("BOXQP: %s.\niterations= %d,  factorizations= %d,  |grad|= %-12.6g, final value= %-12.6g\n",
				result_type[result+1], iter, nFactor, norm, value);
		if( verbosity > 2 )
		{
			tt_printf("solution = ");
			tt_matPrint(x, 1, n);
			tt_printf("clamped (iter:%d)=", iter);
			for( i=0; i<n; i++ )
				tt_printf(" %d", (int)clamped[i]);
			tt_printf("\n");
		}
	}

	// reassign _n as an output
	*_n	= nFree;

	// optional outputs
	if (_R)	*_R = R;
	if (_index)	*_index = index;

	return result;
}
