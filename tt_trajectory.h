#pragma once

#include "tt_util.h"

#pragma pack(push, 1)

#ifdef __cplusplus
extern "C"
{
#endif

#define N_TRJ 26
#define N_TRJ_TIMER 5
#define N_CB_TIMER 12

typedef enum _trjEnableBit
{
	trTIME			= 1<<0,
	trTIMESTEP		= 1<<1,
	trSTATE			= 1<<2,
	trCONTROL		= 1<<3,
	trUSER			= 1<<4,
	trMINIMAL		= trTIME + trTIMESTEP + trSTATE + trCONTROL + trUSER,

	trRESIDUAL		= 1<<5,
	trCOST			= 1<<6,
	trFEATURES		= 1<<7,
	trBASIC			= trMINIMAL + trRESIDUAL + trCOST + trFEATURES,

	trYX			= 1<<8,
	trYU			= 1<<9,
	trYDERIVS		= trYX + trYU,

	trRZ			= 1<<10,
	trDERIVS 		= trRZ + trYDERIVS,

	trCX			= 1<<11,
	trCU			= 1<<12,
	trCXX			= 1<<13,
	trCXU			= 1<<14,
	trCUU			= 1<<15,
	trCDERIVS 		= trCX + trCU + trCXX + trCXU + trCUU,

	trK				= 1<<16,
	trXK 			= 1<<17,
	trFEEDBACK		= trK + trXK,

	trDU 			= 1<<18,
	trVX			= 1<<19,
	trVXX			= 1<<20,
	trRICATTI		= trFEEDBACK + trDU + trVX + trVXX,

	trQX			= 1<<21,
	trQU			= 1<<22,
	trQXX			= 1<<23,
	trQXU			= 1<<24,
	trQUU			= 1<<25,
	trHAMILTONIAN	= trQX + trQU + trQXX + trQXU + trQUU,

	trLQG			= trBASIC + trDERIVS + trCDERIVS + trRICATTI,
	trROLLOUT		= trBASIC + trFEEDBACK,
	trPONTRYAGIN	= trBASIC + trDERIVS + trCDERIVS + trFEEDBACK + trVX + trVXX + trHAMILTONIAN
} trjEnableBit;


/*------------------ trajElement: a single trajectory time-step ----------------------*/
//struct _trj;
//typedef struct _trj trajElement;
typedef struct _trajElement
{
	// trBASIC: basic quantitites
	ttNum	*t;		// time 				(1)
	ttNum	*dt;	// timestep				(1)
	ttNum	*x;		// state				(nx)
	ttNum	*u;		// control				(nu)
	ttNum	*user;  // userdsata			(nuser)
	ttNum	*r;		// residual				(nr)
	ttNum	*c;		// cost	(vector)		(nc)
	ttNum	*f;		// features				(nf)

	// trDERIVS: state and residual derivatives
	ttNum  *yx;	//						(ndx x ndx)
	ttNum  *yu;	//						(ndx x nu)
	ttNum  *rz;	//						(nr  x (ndx+nu))

	// trCDERIVS: cost derivatives
	ttNum  *cx;	//						(ndx)
	ttNum  *cu;	//						(nu)
	ttNum  *cxx;	//						(ndx x ndx)
	ttNum  *cxu;	//						(ndx x nu)
	ttNum  *cuu;	//						(nu  x nu)

	// trFEEDBACK: locally linear policy
	ttNum  *K;		// control gains		(nu  x ndx)
	ttNum  *xk;	// feedback set-point	(nx)

	// trRICATTI: computed by Riccati
	ttNum  *du;	// control modification	(nu)
	ttNum  *Vx;	// cost-to-go gradient	(ndx)
	ttNum  *Vxx;	// cost-to-go Hessian	(ndx x ndx)

	// trHAMILTONIAN: Hamiltonian
	ttNum  *Qx;	//						(ndx)
	ttNum  *Qu;	//						(nu)
	ttNum  *Qxx;	//						(ndx x ndx)
	ttNum  *Qxu;	//						(ndx x nu)
	ttNum  *Quu;	//						(nu x nu)
} trajElement;


extern const char *trjElement_names[N_TRJ];

// negative values are bad
typedef enum _mjoResult
{
	mjOPT_FAIL					= -5,	// unknown failure mode
	mjOPT_DIVERGENCE			= -4,	// numerical divergence
	mjOPT_NEGDEF				= -3,	// negative-definite Hessian
	mjOPT_NO_IMPROVEMENT		= -2,	// no improvement found along search direction
	mjOPT_MUMAX					= -1,	// regularizer reached maximum value
	mjOPT_SUCCESS				= 0,	// succesful step
	mjOPT_TOLFUN				= 1,	// change of objective smaller than tolerance
	mjOPT_TOLX					= 2,	// change of x smaller than tolerance
	mjOPT_TOLG					= 3,	// change of gradient smaller than tolerance
	mjOPT_FULLY_CONSTRAINED		= 4,	// all inequality constraints are active

} mjoResult;

typedef struct _mjoTrace
{
// optimization
	mjoResult outcome;				// outcome of last optimization step
	ttNum cost;					// total cost
	ttNum improvement;				// cost improvement  (old - new)
	ttNum timing[N_TRJ_TIMER];		// optimization timing
	ttNum timingCB[2][N_CB_TIMER];	// internal timing from callbacks
// Levenberg-Marquardt
	ttNum mu, dmu;					// Levenberg-Marquardt regularizer and its scaling coefficient
	ttNum alpha;					// line-search coefficient
	ttNum z;						// actual/expected reduction ratio
// iLQG
	ttNum gradnorm;				// gradient norm
	ttNum dV[2];					// Armijo coeffcients
// MPC
	ttNum policy_lag;				// policy lag
	int winner;						// rollout winner
} mjoTrace;


/*------------- trajectory: an array of trajElement's and their data -----------------*/
typedef struct _trajectory
{
	// struct-array of N trjs
	trajElement* e;

	// allocation flags
	int	flg;

	// sizes
	int N;							// time-steps in the trajectory
	int	nx, nu, nf, nc, nr, ndx;	// size of state, control, cost, residual, diff-space
	int nuser;						// size of userdata
	int nstack, pstack;				// stack size, stack pointer

	// box limits on controls (u)
	ttNum *lower, *upper;			// control limits

	// rollout modifiers
	ttNum feedback;				// feedback scaling coefficient
	ttNum time_shift;				// temporal offset

	mjoTrace trace;					// results from optimization step

	// data buffer
	int		szBuffer;				// size of buffer
	ttNum* buffer;					// start of buffer

} trajectory;

// sizes of different trajectory elements
void trjSizes(int sz[N_TRJ][2], int nx, int nu, int nf, int nr, int nc, int ndx, int nuser);

// allocate and initialize trajectory structure
trajectory* newTrajectory(int nx, int nu, int nf, int nr, int nc, int ndx, int nuser, int N, int flg);

// set trajectory pointers
void setPtrTrajectory(trajectory* T);

// copy one trajectory into another
void copyTrajectory(trajectory* Tout, const trajectory* Tin);

// create a new trajectory and copy into it
trajectory* cloneTrajectory(const trajectory* Tin, int flg);

// interpolate trajectory at time t
void interpTrajectory(trajElement* e, const trajectory* T, ttNum t, int align, int extrap, int ignoreLast);

// clear a trajectory
void zeroTrajectory(trajectory* T);

// de-allocate trajectory
void deleteTrajectory(trajectory* T);

// serialize a trajectory
int saveTrajectory(const trajectory* T, int szbuf, char* buffer);

// de-serialize a trajectory
trajectory* loadTrajectory(trajectory* dest, int szbuf, void* buffer);


#ifdef __cplusplus
}
#endif

#pragma pack(pop)
